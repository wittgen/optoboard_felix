# pylint: disable=line-too-long, invalid-name, import-error
"""Initialize an Optoboard.

Can be used for CLI interface.

Args:
    config_path (str): Relative local path or DB path to configuration file, if not provided it takes a local default file depending on the version of the components on the Optoboard.
    optoboard_serial (str): Optoboard serial number
    vtrx_v (str): VTRx+ quad laser driver version (1.2 or 1.3)
    flx_G (int): FELIX link group
    flx_d (int): FELIX device
    configure (int): if 1, configure all chips on the Optoboard
    commToolName (str): tool to communicate with the Optoboard
    woflxcore (int): for back-compatibility, choose between ic-over-netio (0) and flpgbtconf (1)
    FELIXFOLDER (str): path to FELIX software folder
    YARRFOLDER (str): path to YARR repository
    test_mode (int): 0 is false, 1 is true
    configInDB (int): 0 is false, 1 is true
    CONFIGDB_ADDRESS (int): config database address
Raises:
    ValueError: Optoboard serial is not in driver/components.py
    Exception: local config not available
    Exception: configuration database unavailable
    Exception: could not start felix-core
"""
import argparse
import json
import os
import sys

from collections import OrderedDict
from time import sleep

from driver.Optoboard import Optoboard
from driver.CommWrapper import CommWrapper
from driver.components import components
from driver.log import logger
from driver.CommConfig import CommConfig

from additional_driver.felixyarr import Felix, Yarr
from additional_driver.DB_access import configDbUnavailable, getConfigDataset

from TestOpto import TestOpto

parser = argparse.ArgumentParser(
    description="Initialise Optoboard. Either provide config path or optoboard_serial + vtrx_v + flx_G + flx_d")

parser.add_argument("-config_path", "--config_path", 
                    type=str,
                    default="",
                    help="Relative local path or DB path to configuration file, if not provided it takes a local default file depending on the version of the components on the Optoboard. (default: %(default)s)")

parser.add_argument("-optoboard_serial", "--optoboard_serial",
                    type=str,
                    help="Optoboard serial number",
                    required="-config_path" not in sys.argv)
                    
parser.add_argument("-vtrx_v", "--vtrx_v",
                    type=str,
                    default=None,
                    choices=["1.2", "1.3"],
                    required="-config_path" not in sys.argv,
                    help="VTRx+ quad laser driver version (1.2 or 1.3)")

parser.add_argument("-flx_G", "--flx_G",
                    type=int,
                    default=0,
                    choices=[ G for G in range(0,24)],
                    required="-config_path" not in sys.argv,
                    help="FELIX link group (default: %(default)s)")

parser.add_argument("-flx_d", "--flx_d",
                    type=int,
                    default=0,
                    required="-config_path" not in sys.argv,
                    help="FELIX device (default: %(default)s)")

parser.add_argument("-configure", "--configure",
                    type=int,
                    default=0,
                    choices=[0, 1],
                    help="if 1, configure all chips on the Optoboard (default: %(default)s)")

parser.add_argument("-woflxcore", "--woflxcore",
                    type=int,
                    default=None,
                    choices=[0, 1],
                    help="communicate with subprocesses, does not rely on felix-core (default: %(default)s)")

parser.add_argument("-commToolName", "--commToolName",
                    type=str,
                    default="ic-over-netio",
                    choices=["flpgbtconf", "ic-over-netio", "lpgbt-com"],
                    help="communicate with subprocesses, does not rely on felix-core (default: %(default)s)")

parser.add_argument("-debug", "--debug",
                    type=int,
                    default=0,
                    choices=[0, 1],
                    help="0 is false, 1 is true; type (default: %(default)s)")

parser.add_argument("-FELIXFOLDER", "--FELIXFOLDER",
                    type=str,
                    default=None,
                    help="path to FELIX software folder (default: %(default)s)")

parser.add_argument("-YARRFOLDER", "--YARRFOLDER", type=str, default=None, help="path to YARR repository (default: %(default)s)")

parser.add_argument("-test_mode", "--test_mode",
                    type=int,
                    default=0,
                    choices=[0, 1],
                    help="0 is false, 1 is true (default: %(default)s)")

parser.add_argument("-configInDB", "--configInDB",
                    type=int,
                    default=0,
                    choices=[0, 1],
                    help="0 is false, 1 is true (default: %(default)s)")

parser.add_argument("-CONFIGDB_ADDRESS", "--CONFIGDB_ADDRESS",
                    type=str,
                    default="http://localhost:5000",
                    help="config database address (default: %(default)s)")

args = vars(parser.parse_args())

config_path = str(args["config_path"])
optoboard_serial = str(args["optoboard_serial"])
vtrx_v = str(args["vtrx_v"])
commToolName = str(args["commToolName"])
FELIXFOLDER = str(args["FELIXFOLDER"])
YARRFOLDER = str(args["YARRFOLDER"])
flx_d = int(args["flx_d"])
flx_G = int(args["flx_G"])
debug = bool(int(args["debug"]))
test_mode = bool(int(args["test_mode"]))
configInDB = bool(int(args["configInDB"]))
CONFIGDB_ADDRESS = str(args["CONFIGDB_ADDRESS"])
configure = bool(int(args["configure"]))

# For back-compatibility
if args["woflxcore"] is not None:
    if args["woflxcore"]: 
        commToolName = "flpgbtconf"
    else: 
        commToolName = "ic-over-netio"

logger.warning("Using %s for IC communication", commToolName)

if vtrx_v == "1.2":
    vtrx_v_string = "1_2"
elif vtrx_v == "1.3":
    vtrx_v_string = "1_3"

# Provide the configuration file
logger.info("Current working directory: %s", os.getcwd())

if config_path == "":
    config_provided = False
    try:
        components_opto = OrderedDict(components[optoboard_serial])

    except KeyError:
        logger.error("Provided serial %s is not in the database!", optoboard_serial)
        raise ValueError("Invalid serial provided")

    config_path = "configs/optoboard" + str(components_opto["optoboard_v"]) + "_lpgbtv" + str(components_opto["lpgbt_v"]) + "_gbcr" + str(components_opto["gbcr_v"]) + "_vtrxv" + vtrx_v_string + "_default.json"
else:
    config_provided = True

if not configInDB:
    with open(config_path) as f:
        try:
            logger.info("Initialising config from %s", config_path)
            config_file = json.load(f)

            if config_provided:
               components_opto = OrderedDict(components[config_file["Optoboard"]["serial"]])

        except:
            raise Exception("Unavailable local configuration file!")
else:
    if configDbUnavailable():
        raise Exception("Unavailable configuration database")
    else: config_file = getConfigDataset()

# Initialise communication wrapper and Optoboard object
if config_provided:
    Communication_wrapper = CommWrapper(flx_G=config_file["Optoboard"]["flx_G"], flx_d=config_file["Optoboard"]["flx_d"], lpgbt_master_addr=components_opto["lpgbt_master_addr"], lpgbt_v=components_opto["lpgbt_v"], commToolName=commToolName, test_mode=test_mode)
    opto = Optoboard(config_file, optoboard_serial, config_file["Optoboard"]["vtrx_v"], components_opto, Communication_wrapper, debug)
else:
    Communication_wrapper = CommWrapper(flx_G=flx_G, flx_d=flx_d, lpgbt_master_addr=components_opto["lpgbt_master_addr"], lpgbt_v=components_opto["lpgbt_v"], commToolName=commToolName, test_mode=test_mode)
    opto = Optoboard(config_file, optoboard_serial, vtrx_v, components_opto, Communication_wrapper, debug)
    
logger.info("The optoboard object 'opto' is now available!")


# Try to initialize Felix and Yarr instances
if args["FELIXFOLDER"] is not None:
    try:
        Felix = Felix(FELIXFOLDER)
    except:
        logger.warning("Failed initializing the Felix instance!")
    else: 
        logger.info("The felix object 'Felix' is now available!")

if args["YARRFOLDER"] is not None:
    try:
        Yarr = Yarr(YARRFOLDER)
    except:
        logger.warning("Failed initializing the Yarr instance!")
    else: 
        logger.info("The felix object 'Yarr' is now available!")

# Configure the Optoboard
if configure:
    try:
        if (commToolName == "ic-over-netio") and (args["FELIXFOLDER"] is not None):
            Felix.start_felixcore()
            sleep(4)
    except:
        logger.error("Starting felix-core has failed! Felix instance is probably not initialized!")
        raise Exception("Could not start felix-core")

    logger.info("Starting configuration of Optoboard with serial: %s", optoboard_serial)
    opto.configure()

# Most recent changes
- The parameter "woflxcore" of the script InitOpto.py has been substituted with "commToolName" (string) that accepts "flpgbtconf", "ic-over-netio" or "lpgbt-com" as values.
- Communication via felixClient is now supported. The path to the "bus" folder of the felix software in use which is stored in driver/CommConfig.py and obtained from environment variables. We recommend to source the same felix software used to run felix client. In case of troubles, this is one of the possible causes. 
- Notice that one has to ***source the setup.sh file in the lpgbt-com folder***.

# Optoboard FELIX Configuration Interface
A configuration tool for Optoboards.

This is the backend to the [ITk demonstrator software](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw) but can be used stand-alone.

**Supports**:
- Optoboard V1.1 (lpGBTv0, GBCRv2)
- Optoboard V2.0 (lpGBTv0, GBCRv2)
- Optoboard V2.1 (lpGBTv1, GBCRv2)
- Optoboard V3.0 (lpGBTv1, GBCRv2/GBCRv3)
(all with VTRx+ quad laser driver version 1.2 (on VTRx+ V10) or 1.3)

Please check our documentation: [https://optoboard-system.docs.cern.ch](https://optoboard-system.docs.cern.ch/). Support requests through our [Bern-Optoboard Mattermost](https://mattermost.web.cern.ch/bat-optoboard) (sign up [here](https://mattermost.web.cern.ch/signup_user_complete/?id=778hfkdwfjd5mm899qhs8m9m5w)).

## Prerequisites
Everything hardware related or software that is needed for running a full data transmission setup is summarised in [our documentation](https://optoboard-system.docs.cern.ch/software/). Summarised here are the important parts only related to `optoboard_felix`.

Non-standard Python modules:
```
pip install requests, scipy
```

# How to configure/use an Optoboard
1. Clone this repository:
```
git clone https://gitlab.cern.ch/bat/optoboard_felix.git
```
1. Set up your FELIX environment, configure FELIX card and check link alignment
2. Run `felixcore` in a separate terminal or `felixClient`
3. Specify the communication tool (`ic-over-netio` <-> `felixcore`, `lpgbt-com` <-> `felixClient`) to be used for the IC communication and create an instance of the `Optoboard` class

## Configure through CLI
```
python InitOpto.py [-h]
                   [-config_path CONFIG_PATH]
                   -optoboard_serial OPTOBOARD_SERIAL
                   -vtrx_v {1.2,1.3}
                   -flx_G {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}
                   -flx_d FLX_D
                   [-commToolName {"flpgbtconf","ic-over-netio","lpgbt-com"}]
                   [-woflxcore {0,1}]
                   [-configure {0,1}]
                   [-debug {0,1}]
                   [-FELIXFOLDER FELIXFOLDER]
                   [-YARRFOLDER YARRFOLDER]
                   [-test_mode {0,1}]
                   [-configInDB {0,1}]
                   [-CONFIGDB_ADDRESS CONFIGDB_ADDRESS]
```
For an explanation on the arguments, run `python InitOpto.py -h` or consult the [API](https://optoboard-system.docs.cern.ch/software/optoboard_felix/InitOpto/).

### Examples
Minimal example to configure a full Optoboard using default configuration files and using the Python session afterwards:
```
python -i InitOpto.py -optoboard_serial 30000000 -vtrx_v 1.3 -flx_G 0 -flx_d 0 -configure 1
>>> opto.lpgbt1.check_PUSM_status()
```
or by providing a config like `30000000_test_lpgbtv1_vtrxv1_3.json`:
```
python -i InitOpto.py -config_path configs/30000000_test_lpgbtv1_vtrxv1_3.json -configure 1
```
Remove the `-i` flag if you don't want to start the Python interactive shell and just configure.

Consult our [optoboard_felix API](https://optoboard-system.docs.cern.ch/software/optoboard_felix/) in the Optoboard system docs for available methods and more information.

## Use as module for scripting
Minimal example with multiple Optoboards:
```python
from collections import OrderedDict

from driver.Optoboard import Optoboard, load_custom_config, load_default_config
from driver.CommWrapper import CommWrapper
from driver.components import components

optoboard_serial_1 = "30000000"
optoboard_serial_2 = "2400003"

vtrx_v = "1.3"    # connected VTRx+ quad laser driver version

# get Optoboard chip versions
components_opto_1 = OrderedDict(components[optoboard_serial_1])
components_opto_2 = OrderedDict(components[optoboard_serial_2])

my_config = "configs/my_cool_config.json"

# load configs
config_file_default = load_default_config(components_opto_1, vtrx_v)
config_file_custom = load_custom_config(my_config)

# create wrapper to FELIX
communication_wrapper_1 = CommWrapper(flx_G=0, flx_d=0, lpgbt_master_addr=0x74, lpgbt_v=1)
communication_wrapper_2 = CommWrapper(flx_G=2, flx_d=0, lpgbt_master_addr=0x70, lpgbt_v=1)    # another wrapper for different link and lpGBT address

# initialise Optoboard objects
opto1 = Optoboard(config_file_default, optoboard_serial_1, vtrx_v, components_opto_1, communication_wrapper_1)
opto2 = Optoboard(config_file_custom, optoboard_serial_2, vtrx_v, components_opto_2, communication_wrapper_2)

opto1.configure()    # configure all available chips on the Optoboard
opto2.configure()

# some examples of what one can do
opto1.lpgbt3.check_PUSM_status()    # check PUSM status of lpGBT3

opto1.lpgbt3.phase_training(2)    # train phases on EPRX20 channels of lpGBT3

opto.lpgbt1.bert_by_elink("EPRX20")    # BERT on lpGBT1 for elink 2

opto.lpgbt2.read("EPRX00CHNCNTR", "EPRX00INVERT")    # simple read to check polarity of elink 6 (lpGBT2 ch. 0)

opto.vtrx.read("GCR", None)    # check enabled fibre channels of VTRx+ (only v1.3)

opto1.bertScan("lpgbt3", "gbcr3", 0, 6, 11)    # perform an GBCR3 equalizer scan with BERT on lpGBT3
```


## Config files
The script has default configs according to the lpGBT and VTRx+ quad laser driver version, **please do not change them**. If the user wants to provide its own config please duplicate the default according to your lpGBT and VTRx+ quad laser driver version and change values there. 

Two example configs `00000000_test_lpgbtv1_vtrxv1_2` and `30000000_test_lpgbtv1_vtrxv1_3` are provided for duplication. Note that you can't just change the VTRx+ version in one of them, due to different register maps the configs also have different keys.

# Optional: Connect with FELIX and `YARR` repositories
This is mainly for our internal use, this will be dropped with the ITk demonstrator software.

Some additional functions are implemented in the `additional_driver` folder such as to access the status of FELIX. In particular the functions in `additional_driver/DB_access.py` are used to retrieve a configuration file from the configuration database. The `additional_driver/felixyarr.py` script allows to create a `Felix` object and a `Yarr` object with some useful functions, for example:
```
Felix.felix_diagnosis()
Felix.felix_polarityReboot()
Felix.start_felixcore()
Yarr.setup_yarr()
Yarr.start_yarrscan()
```
To initialize these objects one has to provide the paths to the FELIX and YARR softwares. Note: the Felix and Yarr classes have been designed for Bern internal use.

The `InitOpto.py` script can also be used to initialize an Optoboard object. It initializes an Optoboard object, starts felixcore and configures the board. Example:
```
python -i InitOpto.py -optoboard_serial 00000000 -flx_G 0 -flx_d 0 -vtrx_v 1.2 -FELIXFOLDER <path_to_felix> -YARRFOLDER <path_to_yarr>
```
where the paths end with `/`.


## Optoboard testing
The `TestOpto.py` script contains a function that is imported by `InitOpto.py` and has to be called providing the objects `opto`, `Felix` and `Yarr`, a boolean to select between the bert test and the scan of the frontend, the device=`lpgbtX` with `X=1,2,3,4` on which the test is performed, the uplink number and the length of the bert:
```python
>>> TestOpto(opto, Felix, Yarr, bert_boolean, "device", uplink, BERTMEASTIME)
``` 
Note: the path to the file used to configure the RD53A has to be changed by hand in the file `configs/example_rd53a_setup.json`.

from time import sleep
import os
import json
import subprocess
from datetime import datetime


def TestOpto(opto, Felix, Yarr, bert_boolean, device, uplink, precision,
	frontend_setupFile = "configs/rd53a_test.json", connectivityFile = "configs/example_rd53a_setup.json"):
	"""
	function to automatize the test of the optoboards
	it can performs digital scans of the front end chip through each lpGBT and a short BERT 
	the value of tx is not changed
	only the first chip of the connectivity file is taken into account
	"""
	with open(frontend_setupFile) as f:
		data = json.load(f)
		if bert_boolean:
			data["RD53A"]["GlobalConfig"]["SerSelOut0"] = 2
			data["RD53A"]["GlobalConfig"]["SerSelOut1"] = 2
			data["RD53A"]["GlobalConfig"]["SerSelOut2"] = 2
			data["RD53A"]["GlobalConfig"]["SerSelOut3"] = 2
		else: 
			data["RD53A"]["GlobalConfig"]["SerSelOut0"] = 1
			data["RD53A"]["GlobalConfig"]["SerSelOut1"] = 1
			data["RD53A"]["GlobalConfig"]["SerSelOut2"] = 1
			data["RD53A"]["GlobalConfig"]["SerSelOut3"] = 1
		data_json = json.dumps(data, indent = 4, sort_keys=True)
	with open(frontend_setupFile, "w") as wf:
		wf.write(data_json)


	with open(connectivityFile) as f:
		data = json.load(f)
		config_path = os.getcwd() + "/" + frontend_setupFile
		data["chips"][0]["config"] = config_path
		if device == "lpgbt1":
			data["chips"][0]["rx"] = 0 + 4 * uplink
		elif device == "lpgbt2":
			data["chips"][0]["rx"] = 64 + 4 * uplink
		elif device == "lpgbt3":
			data["chips"][0]["rx"] = 128 + 4 * uplink
		elif device == "lpgbt4":
			data["chips"][0]["rx"] = 192 + 4 * uplink
		else: 
			raise ValueError("Wrong device name!") 		
		data_json = json.dumps(data, indent = 4, sort_keys=True)
	with open(frontend_setupFile, "w") as wf:
		wf.write(data_json)

	output_folder = str(os.getcwd() + "/optoBoardTest/" + opto.optoboard_serial + "/" + device + datetime.now().strftime("_%d%m%Y_%H:%M:%S"))
	if not os.path.exists(output_folder):
		os.makedirs(output_folder)
	
	felixProcess = Felix.start_felixcore()

	sleep(3)

	yarrProcess = Yarr.start_yarrscan("single_rd53a")

	if bert_boolean:
		sleep(5)
		yarrProcess.kill()
		print("Killing yarr")
		print("Performing BERT")
		sleep(1)
		bert_result = eval("opto." + device).bert(uplink+1, 6, precision)
	else:
		yarrProcess.communicate()
		yarrProcess.kill()
		sleep(1)
		scan_result = os.listdir("data/last_scan")
		if len(scan_result)>5:
			print("Copying the scan results")
			for file in scan_result:
				subprocess.Popen(str("mv data/last_scan/" + file + " " + output_folder), shell=True)
		sleep(2)
	subprocess.Popen(str("rm -r data").split())
	felixProcess.kill()
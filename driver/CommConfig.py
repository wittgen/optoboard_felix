import os

CommConfig = {
    "IChandler_config": {"ip": "127.0.0.1", "tx_port": 12340, "rx_port": 12350, "tx_tag": 17, "rx_tag": 29},
    "lpgbt_com_config": {"local_eth": "eth0", "bus_dir": str(os.environ.get("VIRTUAL_ENV") or "") + "/../bus", "tx_tag": 17, "rx_tag": 29},
}

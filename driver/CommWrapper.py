# pylint: disable=line-too-long, invalid-name, import-error
"""Communication wrapper module."""

import re
import subprocess
import sys
import time

from driver.log import logger
from driver.CommConfig import *
try:
    from register_maps import lpgbt_register_map_v1
    from register_maps import lpgbt_register_map_v0
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with import of lpGBT register maps!")

try:
    from libic_comms import IChandler
except ImportError:
    logger.warning("IChandler library not found, check if installed with FELIX software! Check the README on how to install.")
try:
    import pyLpGBT
    from pyconnect import *
    class ICMaster(pyLpGBT.ICMaster) :
        def read_reg(self, addr, size = 1) :
            return self.read(addr, size)
        def write_reg(self, addr, value) :
            self.write(addr, bytes(value))
except ImportError:
    logger.warning("pyLpGBT or pyconnect libraries for lpgbt-com not found! Source lpgbt-com/setup.sh! ") 

class CommWrapper():
    """Class for handling the access to the FELIX communication wrapper.

    Communication happens either through the IChandler library (to be installed additionally, check README)
    or the FELIX low level tool flpgbtconf.
    """

    def __init__(self, flx_d, flx_G, lpgbt_master_addr, lpgbt_v, commToolName="ic-over-netio", test_mode=False):
        """Initialize the CommWrapper object.

        Args:
            flx_d (int): FELIX device to use (please refer to the FELIX user manual)
            flx_G (int): GBT link number (please refer to the FELIX user manual)
            lpgbt_master_addr (hex): I2C address of the I2C controller
            lpgbt_v (int): lpGBT version (0 or 1)
            commToolName (string): select the communication wrapper to be used
                Current possible values: "flpgbtconf", "ic-over-netio", "lpgbt-com"
            test_mode (bool, optional): set to True if hardware is not available and to test software.
                Returns sent register data for write_read and 4 for all read operations.

        Attributes:
            flx_d (int): FELIX device to use (please refer to the FELIX user manual)
            flx_G (int): GBT link number (please refer to the FELIX user manual)
            lpgbt_master_addr (hex): I2C address of the I2C controller
            lpgbt_v (int): lpGBT version (0 or 1)
            commToolName (string): select the communication wrapper to be used
                Current possible values: "flpgbtconf", "ic-over-netio", "lpgbt-com"
            test_mode (bool, optional): set to True if hardware is not available and to test software.
                Returns sent register data for write_read and 4 for all read operations.
            lpgbt_reg_map (class): lpGBT register map imported
        """
        self.flx_d = flx_d
        self.flx_G = flx_G
        self.lpgbt_master_addr = lpgbt_master_addr
        self.lpgbt_v = lpgbt_v
        self.commToolName = commToolName
        self.test_mode = test_mode
        if self.lpgbt_v:
            self.lpgbt_reg_map = lpgbt_register_map_v1.LpgbtRegisterMapV1
        else:
            self.lpgbt_reg_map = lpgbt_register_map_v0.LpgbtRegisterMapV0

        if commToolName == "ic-over-netio":
            if "libic_comms" not in sys.modules:
                logger.error("IChandler library libic_comms not found, setting commToolName to \"flpgbtconf\"! To configure you now need to disable felixcore and use \"flpgbtconf\" or install the library!")
                self.commToolName = "flpgbtconf"
            else:
                self.ICHandler = IChandler(CommConfig["IChandler_config"]["ip"], CommConfig["IChandler_config"]["tx_port"]+int(self.flx_d), CommConfig["IChandler_config"]["rx_port"], 
                    CommConfig["IChandler_config"]["tx_tag"] + 64*int(self.flx_G), CommConfig["IChandler_config"]["rx_tag"] + 64*int(self.flx_G), False, self.lpgbt_v, self.lpgbt_master_addr)
        elif commToolName == "lpgbt-com":
            if "pyLpGBT" not in sys.modules:
                logger.error("pyLpGBT library libic_comms not found, setting commToolName to \"flpgbtconf\"! To configure you now need to disable felixclient and use \"flpgbtconf\" or install the library!")
                self.commToolName = "flpgbtconf" 
            else:                                   
                self.ctrl = FelixClientCtrl(CommConfig["lpgbt_com_config"]["local_eth"], CommConfig["lpgbt_com_config"]["bus_dir"], "FELIX",  0x1000000000000000 | (CommConfig["lpgbt_com_config"]["rx_tag"] << 16),  0x1000000000008000 | (CommConfig["lpgbt_com_config"]["tx_tag"] << 16), 4, False)
                
                time.sleep(0.4)
                try:
                    self.ctrl.connect()
                except:
                    logger.fatal("Check if the specified path to the folder \"/bus\" of the FELIX software in use is correct and if FELIX is running!")

                time.sleep(0.4)

                self.ic_lpgbt_com = ICMaster(self.ctrl)
                self.ic_lpgbt_com.lpgbt_ver = lpgbt_v
                self.ic_lpgbt_com.i2c_addr  = lpgbt_master_addr 

        logger.info("CommWrapper object initialised!")

    def comm_wrapper(self, reg_name, reg_data, readback_bool=True):
        """Communicate with FELIX.

        Tries 3 communication attempts before raising an Exception.

        Args:
            reg (str): register name
            reg_data (int): register data to write
            readback_bool (bool): if False, don't try to read back the written value

        Returns:
            read_reg (str): read register according to FELIX flpgbtconf tool
            read (int): read-back
        """
        max_tries = 1

        if self.test_mode:
            if reg_data is None:
                reg_data = 4
            return reg_name, reg_data

        if self.commToolName == "flpgbtconf":

            flpgbtconf_tries = 0

            logger.debug("comm_wrapper - parsed - flx_G: %s, flx_d: %s, lpgbt_v: %s, lpgbt_master_addr: %s, reg_data: %s", self.flx_G, self.flx_d, self.lpgbt_v, self.lpgbt_master_addr, reg_data)

            process_list = ["flpgbtconf"]

            if self.lpgbt_v:
                process_list.append("-1")

            process_list.extend(["-G", str(self.flx_G), "-I", hex(self.lpgbt_master_addr), "-d", str(self.flx_d)])

            process_list.append(reg_name)


            if reg_data is not None:
                process_list.append(hex(reg_data))

            logger.debug("comm_wrapper - process_list: %s", process_list)

            while flpgbtconf_tries<max_tries:
                output = subprocess.Popen(process_list, stdout=subprocess.PIPE)

                output_lines = output.stdout.readlines()

                logger.debug("comm_wrapper - readlines() output - output_lines: %s", output_lines)

                output_last_line = str(output_lines[-1])
                logger.debug("comm_wrapper - output_last_line: %s", output_last_line)

                if not readback_bool:
                    return "Write-only operation"

                if "Reply not received" in output_last_line:

                    flpgbtconf_tries += 1

                    logger.warning("Reply not received from FELIX! - tried %s time(s)", flpgbtconf_tries)

                    if flpgbtconf_tries>max_tries-1:
                        logger.error("comm_wrapper - no reply from FELIX. Aborting, check link, Optoboard serial, config file or FELIX settings!")
                        raise Exception("No FELIX connection.")
                else: break

            read_reg = re.search("\'(.+?)\:", output_last_line).group(1)
            read = int(re.search("\((.+?)\)", output_last_line).group(1))

            logger.debug("comm_wrapper - returning - read_reg: %s, read: %s", read_reg, read)

            flpgbtconf_tries = 0

            return read_reg, read

        elif self.commToolName == "ic-over-netio":
            ICHandler_tries = 0

            while ICHandler_tries<max_tries:

                try:
                    if reg_data is not None:
                        self.ICHandler.sendRegs({reg_name:[int(reg_data)]})
                    if not readback_bool:
                        return "Write-only operation"

                    readAll = self.ICHandler.readRegs([reg_name])
                    read_reg = readAll[0][0]
                    read_back = readAll[0][1]

                except:
                    ICHandler_tries += 1
                    logger.warning("Reply not received from FELIX! - tried %s time(s)", ICHandler_tries)

                    if ICHandler_tries>max_tries-1:
                        logger.error("comm_wrapper - no reply from FELIX. Aborting, check link, Optoboard serial, config file or FELIX settings!")
                        raise Exception("No FELIX connection.")

                else: break

            logger.debug("comm_wrapper - ICHandler - reg_data: %s, read_reg: %s, read_back: %s", reg_data, read_reg, read_back)

            return read_reg, read_back

        elif self.commToolName == "lpgbt-com":
            lpgbt_com_tries = 0

            while lpgbt_com_tries<max_tries:

                try:
                    if reg_data is not None:
                        self.ic_lpgbt_com.write_reg(int(self.lpgbt_reg_map().Reg[reg_name].value), [int(reg_data)])
                    if not readback_bool:
                        return "Write-only operation"

                    # read operation
                    read_back = self.ic_lpgbt_com.read_reg(self.lpgbt_reg_map().Reg[reg_name].value)

                except:
                    lpgbt_com_tries += 1
                    logger.warning("Reply not received from FELIX! - tried %s time(s)", lpgbt_com_tries)

                    if lpgbt_com_tries>max_tries-1:
                        logger.error("comm_wrapper - no reply from FELIX. Aborting, check link, Optoboard serial, config file or FELIX settings!")
                        raise Exception("No FELIX connection.")

                else: break
            
            read_back = int.from_bytes(read_back, "little")
            logger.debug("comm_wrapper - lpgbt_com_tries - reg_data: %s, read_reg: %s, read_back: %s", reg_data, reg_name, read_back)

            return reg_name, read_back
            
        else:
            logger.fatal("The provided name for the communication library doesn't exist!!")


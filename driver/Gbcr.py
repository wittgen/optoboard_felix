# pylint: disable=line-too-long, invalid-name, import-error
"""GBCR module."""

from driver.log import logger
from driver.Hardware import Hardware, ordered_configuration

try:
    from register_maps import gbcr2_register_map
    from register_maps import gbcr3_register_map
except (ImportError, ModuleNotFoundError):
    logger.critical("Error in importing GBCR register maps!")



class Gbcr(Hardware):
    """Class for controlling the GBCRs on an Optoboard.

    Attributes:
        device_type (str): "gbcr"
        master (bool): False because it can only be controlled via the lpGBT master
    """

    device_type = "gbcr"
    master = False

    def __init__(self, Communication_wrapper, device, address, master, I2C_master, config_file, gbcr_v):
        """Initialize of GBCR object.

        Args:
            Communication_wrapper (class): low level access to FELIX access
            device (str): gbcr1, gbcr2, gbcr3, gbcr4
            address (hex): I2C address of the GBCR
            master (bool): should always be False
            I2C_master (int): I2C controller to use (can be 0, 1, 2)
            config_file (dict): configuration values from JSON config
            gbcr_v (int): GBCR version

        Attributes:
            gbcr_v (int): GBCR version, either 2 or 3
            gbcr_reg_map (class): imported GBCR register map module
            config_file (dict): configuration values from JSON config
        """
        super().__init__(Communication_wrapper, device, address, master, I2C_master, config_file)

        self.gbcr_v = gbcr_v

        if self.gbcr_v == 2:
            self.gbcr_reg_map = gbcr2_register_map.GBCRRegisterMap
        elif self.gbcr_v ==3:
            self.gbcr_reg_map = gbcr3_register_map.GBCRRegisterMap
        else:
            raise Exception("gbcr version is nor 2 neither 3")

        self.config_file["gbcr"] = ordered_configuration(self.config_file["gbcr"], self.gbcr_reg_map)
        self.config_file.pop("power_up_registers_master")
        self.config_file.pop("power_up_registers_slave")
        self.config_file.pop("lpgbt")
        self.config_file.pop("vtrx")

        logger.info("%s object initialised!", self.device)

    def configure(self, validate = False):
        """Configure all 32 registers of the GBCR.

        Compares written values with a read back.

        Args:
            validate (bool, optional): set True if a read back should be performed
        Returns:
            None
        """
        data = []

        for reg in self.config_file["gbcr"]:
            data.append(self.construct_reg_data(reg))

        logger.debug("gbcr.configure - data: %s", data)

        self.write_32bytes(data)

        if validate:
            read_back = self.read_32bytes()

            if read_back==data:
                logger.error("Read back %s is not identical to data written %s", read_back, data)
                raise ValueError("Configure problem, read back not identical to written data!")

        logger.info("GBCR configured!")

    def readAll(self):
        """Read out all registers of the GBCR.

        Returns:
            read (list): read-out registers, values are decimal
        """
        registers = []

        logger.debug("GBCR register map: %s", self.gbcr_reg_map.__dict__)

        for reg in self.gbcr_reg_map.__dict__:
            if (reg != "Reg") and (reg[0] != "_"):
                registers.append(reg)

        read = self.read_32bytes()
        logger.debug("gbcr.readAll - registers: %s, read: %s", registers, read)

        return read

    def set_retiming_mode(self, channel, phase = 5):
        """Set the retiming mode of a GBCR channel with phase.

        Args:
            channel (int): channel of GBCR
            phase (int, optional): phase to set, default 5
        Returns:
            None
        """
        logger.info("Enabling retiming mode for %s channel %s..", self.device, channel)

        # setting RXEN=1, RXSETCM=1, RXENTERMINATION=1, DISTX=1
        self.write_read("LVDSRXTX", None, 113)
        self.write_read("PHASESHIFTER0", "DLLENABLE", 1)

        # setting default phase to 5
        if channel == 0:
            self.write_read("PHASESHIFTER5", "DLLCLOCKDELAYCH1", phase)
        elif channel in [1,2]:
            self.write_read("PHASESHIFTER4", "DLLCLOCKDELAYCH"+str(channel+1), phase)
        elif channel in [3,4]:
            self.write_read("PHASESHIFTER3", "DLLCLOCKDELAYCH"+str(channel+1), phase)
        elif channel == 5:
            self.write_read("PHASESHIFTER2", "DLLCLOCKDELAYCH"+str(channel+1), phase)

        logger.info("%s channel %s retiming mode enabled and set to phase %s!", self.device, channel, phase)

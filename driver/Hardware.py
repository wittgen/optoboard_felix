# pylint: disable=line-too-long, invalid-name, import-error
"""Hardware module."""

import inspect

from collections import OrderedDict

from driver.log import logger

def ordered_configuration(configuration_4_device, regMap):
    """Order configuration.

    ?

    Attributes:
        configuration_4_device (?): ?
        regMap (class): register map

    Returns:
        dictionary with keys and values
    """

    logger.debug("configuration_4_device: %s", configuration_4_device)

    def myOrder(e):
        return e["reg_address"]

    ordered_configuration = []
    for reg in configuration_4_device:
        dict4_ordering = {}
        dict4_ordering["reg_name"] = reg
        dict4_ordering["reg_address"] = getattr(regMap, str(reg)).address
        if type(configuration_4_device).__name__ == "dict":
            dict4_ordering["content"] = configuration_4_device[reg]
        ordered_configuration.append(dict4_ordering)

    ordered_configuration.sort(key=myOrder)

    if type(configuration_4_device).__name__ == "dict":
        return OrderedDict({x["reg_name"]:x["content"] for x in ordered_configuration})
    else:
        return [x["reg_name"] for x in ordered_configuration]



class Hardware():
    """Class for handling the write and read with the FELIX communication wrapper.

    Attributes:
        I2C_* (hex): I2C controller commands used for writing to slave devices on the Optoboard.
        FREQ (int): I2C controller bus speed
        SCLDRIVE (int): I2C controller SCLDriveMode (see also lpGBTv1 manual ch. 12.2.1)
    """

    I2C_WRITE_CR = 0x0
    I2C_WRITE_MSK = 0x1
    I2C_1BYTE_WRITE = 0x2
    I2C_1BYTE_READ = 0x3
    I2C_1BYTE_WRITE_EXT = 0x4
    I2C_1BYTE_READ_EXT = 0x5
    I2C_1BYTE_RMW_OR = 0x6
    I2C_1BYTE_RMW_XOR = 0x7
    I2C_W_MULTI_4BYTE0 = 0x8
    I2C_W_MULTI_4BYTE1 = 0x9
    I2C_W_MULTI_4BYTE2 = 0xA
    I2C_W_MULTI_4BYTE3 = 0xB
    I2C_WRITE_MULTI = 0xC
    I2C_READ_MULTI = 0xD
    I2C_WRITE_MULTI_EXT = 0xE
    I2C_READ_MULTI_EXT = 0xF
    FREQ = 2
    SCLDRIVE = 0

    def __init__(self, Communication_wrapper, device, address, master, I2C_master, config_file):
        """Initialise the Hardware object.

        Args:
            Communication_wrapper (class): low level access to FELIX access
            device (str): device to write/read
            address (hex): I2C address of device
            master (bool): True if lpGBT master, False if anything else
            I2C_master (int): I2C controller number to use
            config_file (dict): dictionary with list of registers to be configured and their values

        Attributes:
            comm_wrapper (class): low level access to FELIX access
            device (str): device to write/read
            address (hex): I2C address of device
            master (bool): True if lpGBT master, False if anything else
            I2C_master (int): I2C controller number to use
            config_file (dict): dictionary with list of registers to be configured and their values
            device_type (str): can be lpgbt or gbcr or vtrx
            device_nr (int): number of the device if lpGBT or GBCR, can be 1, 2, 3 or 4
        """
        self.comm_wrapper = Communication_wrapper.comm_wrapper
        self.device = device
        self.address = address
        self.master = master
        self.I2C_master = I2C_master
        self.config_file = config_file

        if device[:-1] in ["lpgbt", "gbcr"]:
            self.device_type = device[:-1]
            self.device_nr = int(device[-1:])

        else:
            self.device_type = device

    def validate_reg_data(self, reg, reg_field, reg_data):
        """Validate a provided reg_data according the allowed values from the register map class.

        No return but raises a ValueError if the value is not allowed.

        Args:
            reg (str): register name as in the register maps
            reg_field (str): register field name as in the register maps.
            reg_data (int):	register or register field data to be written

        Raises:
            ValueError: entered register data is not possible
        """
        logger.debug("validate_reg_data - parsed - reg: %s, reg_field: %s, reg_data: %s", reg, reg_field, reg_data)

        reg_field_class = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), reg_field)
        length = getattr(reg_field_class, "length")

        if not reg_field_class.validate(reg_data):
            logger.error("Entered register value %s for %s_%s in config is not valid! Available integer values <= %s", reg_data, reg, reg_field, 2**length)
            raise ValueError("Wrong register data entered.")

    def construct_reg_data(self, reg):
        """Construct the full 8 bits reg_data according to the register field values in config_file and the offsets/lengths from the register map class.

        Args:
            reg (str): register name as in the register maps

        Returns:
            reg_data (int): register data constructed
        """
        reg_data = 0

        logger.debug("construct_reg_data - parsed - reg: %s", reg)

        for setting,value in self.config_file[self.device_type][reg].items():

            reg_field_class = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), setting)
            offset = getattr(reg_field_class, "offset")

            if self.device_type == "vtrx":
                self.validate_reg_data(reg, setting, value)
                reg_data = reg_data | value << offset
            else:
                self.validate_reg_data(reg, setting, value[self.device_nr-1])
                reg_data = reg_data | value[self.device_nr-1] << offset

        logger.debug("construct_reg_data - returning reg_data: d\"%s (h\"%s / b\"%s)", reg_data, hex(reg_data), bin(reg_data)[2:].zfill(8))

        return reg_data

    def write_read(self, reg, reg_field, reg_data):
        """Write and read back a register of a device on the Optoboard.

        Args:
            reg (str): register name as in the register maps
            reg_field (str): register field name as in the register maps.
                If None is provided reg_data gets sent to the device as a full byte
                If a reg_field string is provided only the field gets updated.
            reg_data (int):	register or register field data to be written

        Returns:
            read_back (int): read back value of the register written

        Raises:
            ValueError: if 4 attempts of writing have been attempted and the read back is not identical to the sent reg_data
        """
        count_mismatch = 0
        while count_mismatch < 4:

            reg = reg.upper()
            if reg_field is not None: reg_field = reg_field.upper()

            if reg_field is not None:

                logger.debug("Writing register field %s", reg_field)
                self.validate_reg_data(reg, reg_field, reg_data)

                logger.disabled = True
                current_reg_data = self.read(reg, None)      # gets the currently stored register value
                logger.disabled = False

                reg_data_neg = 0
                reg_class = getattr(eval("self." + self.device_type + "_reg_map"), reg)

                for setting in inspect.getmembers(reg_class):
                    if inspect.isclass(setting[1]) and not "__" in setting[0]:
                        if not setting[0] == reg_field:
                            reg_data_neg = reg_data_neg | getattr(getattr(reg_class, setting[0]), "bit_mask")   # creates an integer from 1 byte with all 1s but the size and offset of reg_field

                new_reg_data = current_reg_data & reg_data_neg      # takes the current_reg_data and sets the bits from reg_field to 0

                logger.debug("write_read - current_reg_data: %s, reg_data_neg: %s, new_reg_data: %s", bin(current_reg_data)[2:].zfill(8), bin(reg_data_neg)[2:].zfill(8), bin(new_reg_data)[2:].zfill(8))

                new_reg_data = new_reg_data | reg_data << getattr(getattr(reg_class, reg_field), "offset")      # writes the reg_field with the provided reg_data

                logger.debug("write_read - changed reg_data from %s to %s", bin(current_reg_data)[2:].zfill(8), bin(new_reg_data)[2:].zfill(8))

                parsed_reg_data = reg_data
                reg_data = new_reg_data     # set reg_data to new 1 byte register data otherwise the reg_field data gets written to the full register

            ### write-read for lpGBT master over IC
            if self.master:

                read_reg, read_back = self.comm_wrapper(reg, reg_data)

                if reg_field is not None:

                    reg_field_class = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), reg_field)
                    read_back = read_back >> getattr(reg_field_class, "offset") & (2**getattr(reg_field_class, "length")-1)
                    read_reg = reg + "_" + reg_field


                logger.debug("write_read - lpgbt master - returning - reg: %s, reg_field: %s, read_reg %s, read_back: %s", reg, reg_field, read_reg, read_back)


            ### write-read to slave device with lpgbt master I2C controller
            else:

                reg_addr = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), "address")

                # configure I2C transaction
                if self.device_type == "lpgbt":        # differentiation because lpGBT slave reg address can be larger than 256, address gets split into Data0 and Data1
                    NBYTE = 3
                else:
                    NBYTE = 2

                logger.debug("write-read - I2C slave - NBYTE: %s", NBYTE)

                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

                # Send register address and register value
                if self.device_type == "lpgbt":
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", divmod(reg_addr,0x100)[1])    # Lower half of register address
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA1", divmod(reg_addr,0x100)[0])    # Upper half of register address
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA2", reg_data)        # Register data to send
                else:
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA1", reg_data)

                self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)    # Initiate send of register address and register value

                # reading status register for read of register
                status_value = self.comm_wrapper("I2CM" + str(self.I2C_master) + "STATUS", None)
                self.status_info(status_value)     # Read status register

                ## Read for I2C device
                logger.disabled = True
                read_back = self.read(reg, reg_field)
                logger.disabled = False

            if reg_field is not None:
                read_reg = reg + "_" + reg_field
                reg_data = parsed_reg_data
            else:
                read_reg = reg      # would otherwise return "I2CM" + str(I2C_master) + "READ15"

            if not read_back == reg_data:
                logger.warning("Read back value is not identical to sent data! Register: %s, sent: %s, read back: %s. Trying again...", read_reg, reg_data, read_back)
                count_mismatch +=1
            else:
                if count_mismatch > 0:
                    logger.info("Read back value and sent value are identical after %s tries!", count_mismatch+1)
                break

            logger.debug("Written to %s - %s: %s (%s, %s)", self.device, read_reg, read_back, hex(read_back), bin(read_back))

        if count_mismatch == 4:
            logger.error("The read back: %s is not identical to the sent reg_data: %s", read_back, reg_data)
            raise ValueError("The read back value is not identical to sent data! 4 attempts were tried!")

        return read_back

    def write_32bytes(self, reg_val):
        """Use lpGBT I2C controllers multi byte write for writing 32 bytes to GBCR or VTRx+.

        Method has no read back!

        Args:
            reg_val (list): values to write

        Raises:
            ValueError: if non GBCR or VTRx+ device or if not 32 bytes
            ValueError: not 32 register data values provided
        """
        logger.debug("write_32bytes - parsed - data: %s", reg_val)

        if not self.device_type in ["gbcr", "vtrx"]:
            logger.error("Trying write_32bytes with non GBCR or VTRx+ device, aborting..")
            raise ValueError("device wrong")

        if not len(reg_val) == 32:
            logger.error("Parsed a list of register data with less or more entries than 32. Length: %s", len(reg_val))
            raise ValueError("List of register values not of length 32!")

        NBYTE = 16
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

        reg_addr = 0
        # Send register address and register value
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

        for byte in range(0,4):
            for data in range(0,4):
                if byte==0 and data==0: continue    # skip first DATA0 due to register address
                logger.debug("write_32bytes - reg_addr: %s, BYTE%s, DATA%s, reg_val: %s", reg_addr, byte, data, reg_val[reg_addr])
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA"+str(data), reg_val[reg_addr])
                reg_addr += 1

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0+byte)

        self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)

        # NBYTE = 15
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

        # Send register address and register value
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

        for byte in range(0,4):
            for data in range(0,4):
                if byte==0 and data==0: continue    # skip first DATA0 due to register address
                logger.debug("write_32bytes - reg_addr: %s, BYTE%s, DATA%s, reg_val: %s", reg_addr, byte, data, reg_val[reg_addr])
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA"+str(data), reg_val[reg_addr])
                reg_addr += 1

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0+byte)

        self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)

        ## write last 3 registers
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

        # Send register address and register value
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

        for data in range(1,3):
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA"+str(data), reg_val[reg_addr])
            logger.debug("write_32bytes - reg_addr: %s, BYTE0, DATA%s, reg_val: %s", reg_addr, data, reg_val[reg_addr])
            reg_addr += 1

        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)

    def read(self, reg, reg_field):
        """Read a register of a device on the Optoboard.

        Args:
            reg (str): register name as in the register maps
            reg_field (str): register field name as in the register maps.
                If None is provided reg_data gets sent to the device as a full byte,
                If a reg_field string is provided only the field gets updated.

        Returns:
            read (int): read value of the register
        """
        reg = reg.upper()
        if reg_field is not None: reg_field = reg_field.upper()
        logger.debug("read - parsed - reg: %s, reg_field: %s", reg, reg_field)

        ### Read for lpGBT master over IC
        if self.master:

            read_reg, read = self.comm_wrapper(reg, None)

        ### Read with lpGBT master I2C controller on Optoboard
        else:
            reg_addr = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), "address")

            if self.device_type == "lpgbt":
                NBYTE = 2
            else:
                NBYTE = 1

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

            # Send register address
            if self.device_type == "lpgbt":
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", divmod(reg_addr,0x100)[1])    # Lower half of register address
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA1", divmod(reg_addr,0x100)[0])    # Upper half of register address

            else:
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)    # Initiate send of register address and register value

            # read answer from I2C device
            NBYTE = 1
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_READ_MULTI)

            #if not Optoboard.test_mode:
                # Reading status register for read of register
                #status_value = self.comm_wrapper("I2CM" + str(self.I2C_master) + "STATUS, None)
                #self.status_info(status_value)     # Read status register

            # Read slave answer from LpGBT master register
            read_reg, read = self.comm_wrapper("I2CM" + str(self.I2C_master) + "READ15", None)

        if reg_field is not None:

            reg_field_class = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), reg_field)
            read = read >> getattr(reg_field_class, "offset") & (2**getattr(reg_field_class, "length")-1)

            logger.debug("read - reg_field is not None - offset: %s, length: %s", getattr(reg_field_class, "offset"), getattr(reg_field_class, "length"))

            read_reg = reg + "_" + reg_field
        else:
            read_reg = reg      # would otherwise return "I2CM" + str(I2C_master) + "READ15"

        logger.debug("Read from %s - %s: %s (%s, %s)", self.device, read_reg, read, hex(read), bin(read))

        return read

    def read_32bytes(self):
        """Use lpGBT I2C controllers multi byte read for reading 32 bytes to GBCR or VTRx+.

        Returns:
            read (list): values read

        Raises:
            ValueError: if non GBCR or VTRx+ device
        """
        read = []

        if self.device_type not in ["gbcr", "vtrx"]:
            logger.error("Trying read_32bytes with non GBCR or VTRx+ device: %s! Aborting..", self.device_type)
            raise ValueError("device wrong")

        ## read first 16 registers
        NBYTE = 1
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

        # Send register address 0
        reg_addr = 0
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)    # Initiate send of register address

        # read registers with MULTI_BYTE_READ
        NBYTE = 16
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

        self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_READ_MULTI)

        while True:
            if self.status_info(self.comm_wrapper("I2CM" + str(self.I2C_master) + "STATUS", None)):
                break

        # read GBCR answers from lpGBT master registers
        for r in range(NBYTE-1, -1, -1):
            read.append(self.comm_wrapper("I2CM" + str(self.I2C_master) + "READ"+str(r), None)[1])

        ## read registers 16-32 from GBCR
        NBYTE = 1
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

        # Send register address 16 to start from second half
        reg_addr = 16
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)    # Initiate send of register address

        # read registers with MULTI_BYTE_READ
        NBYTE = 16
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

        self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
        self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_READ_MULTI)

        while True:
            if self.status_info(self.comm_wrapper("I2CM" + str(self.I2C_master) + "STATUS", None)):
                break

        # read GBCR answers from lpGBT master registers
        for r in range(NBYTE-1, -1, -1):
            read.append(self.comm_wrapper("I2CM" + str(self.I2C_master) + "READ"+str(r), None)[1])

        for addr, r in enumerate(read):
            # logger.debug("%s: %s (%s, %s)", str(self.gbcr_reg_map.Reg(addr))[4:], r, hex(r), bin(r))
            logger.debug("addr: %s, reg_val: %s (%s, %s)", addr, r, hex(r), bin(r))

        logger.debug("len(read): %s", len(read))

        return read

    def status_info(self, status):
        """Check whether I2C transaction was successful.

        Depends on I2CMXStatus register of lpGBT master.

        Args:
            status (int): value of the I2CMxSTATUS register

        Returns:
            passed (bool): True if transmission status register is saying success

        Raises:
            Exception: I2C controller clock disabled
            Exception: the last transaction was not acknowledged by the I2C slave
            Exception: invalid command sent to I2C channel
            Exception: SDA line is pulled low '0' before initiating a transaction
            Exception: I2C transmission error, transaction not successful
        """
        passed = False
        status = bin(status[1])[2:].zfill(8)    # Most significant bit now is bit 7, least significant bit now is bit 0
        logger.debug("status_info - status: %s", status)

        if int(status[0]):
            logger.error("40 MHz clock of the I2C master channel is disabled.")
            raise Exception("I2C transmission error")

        elif int(status[1]):
            logger.error("Last transaction not acknowledged by the I2C slave.")
            raise Exception("I2C transmission error")

        elif int(status[2]):
            logger.error("Invalid command sent to I2C channel.")
            raise Exception("I2C transmission error")

        elif int(status[4]):
            logger.error("SDA line is pulled low '0' before initiating a transaction.")
            raise Exception("I2C transmission error")

        elif int(status[5]):
            passed = True

        else:
            logger.error("I2C transmission error, transaction not successful! status: %s", status)
            raise Exception("I2C transmission error")

        return passed

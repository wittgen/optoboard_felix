# pylint: disable=line-too-long, invalid-name, import-error
"""Lpgbt module."""

import time

from time import sleep

from driver.Hardware import Hardware, ordered_configuration
from driver.log import logger, LoggerFormat

try:
    from register_maps import lpgbt_register_map_v1
    from register_maps import lpgbt_register_map_v0
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with import of lpGBT register maps!")

class Lpgbt(Hardware):
    """Class for controlling the lpGBTs on an Optoboard.

    Attributes:
        device_type (str): "lpgbt"
    """

    device_type = "lpgbt"

    def __init__(self, Communication_wrapper, device, address, master, I2C_master,
                config_file, optoboard_serial, optoboard_v, lpgbt_v, efused):
        """Initialize of lpGBT object.

        Args:
            Communication_wrapper (class): low level access to FELIX access
            device (str): lpgbt1, lpgbt2, lpgbt3, lpgbt4
            address (hex): I2C address of the lpGBT
            master (bool): True if lpGBT master
            I2C_master (int): I2C controller to use (can be 0, 1, 2)
            config_file (dict): configuration values from JSON config
            optoboard_serial (str): provided Optoboard serial number
            optoboard_v (float): Optoboard serial
            lpgbt_v (int): lpGBT version
            efused (int): 1 if Optoboard is efused

        Attributes:
            lpgbt_v (int): lpGBT version
            optoboard_serial (str): provided Optoboard serial number
            optoboard_v (float): Optoboard serial
            efused (int): 1 if Optoboard is efused
            state (str): PUSM status of lpGBT, None if lpGBT not configured
            ready (bool): True if PUSM status is ready, Flase if anything else
            lpgbt_reg_map (class): lpGBT register map imported
            master (bool): True if lpGBT master
            config_file (dict): configuration values from JSON config
        """
        super().__init__(Communication_wrapper, device, address, master, I2C_master, config_file)

        self.lpgbt_v = lpgbt_v
        self.optoboard_serial = optoboard_serial
        self.optoboard_v = optoboard_v
        self.efused = efused

        self.state = None
        self.ready = False

        if self.lpgbt_v:
            self.lpgbt_reg_map = lpgbt_register_map_v1.LpgbtRegisterMapV1
        else:
            self.lpgbt_reg_map = lpgbt_register_map_v0.LpgbtRegisterMapV0

        if self.master:

            self.config_file["power_up_registers_master"] = ordered_configuration(self.config_file["power_up_registers_master"], self.lpgbt_reg_map)
            self.config_file.pop("power_up_registers_slave")
            self.config_file.pop("gbcr")
            self.config_file.pop("vtrx")

        else:

            self.config_file["power_up_registers_slave"] = ordered_configuration(self.config_file["power_up_registers_slave"], self.lpgbt_reg_map)
            self.config_file.pop("power_up_registers_master")
            self.config_file.pop("gbcr")
            self.config_file.pop("vtrx")

        logger.info("%s object initialised!", self.device)

    def configure(self):
        """Configure lpGBT according to power_up_registers_* list in config JSON.

        Has some conditions on individual Optoboards as there might have been problems during efusing

        Returns:
            ready (bool): True if lpGBT ready
        """
        if ((self.optoboard_serial=="08000000") or ("524d" in self.optoboard_serial) or (self.optoboard_v==1)) and self.master:        # condition because wrongly efused bits, this will force the lpgbt back into configuration mode
            logger.info("Force PUSM state to PAUSE_FOR_DLL_CONFIG due to efusing errors..")
            self.write_read("POWERUP4", None, 0xa3)
            self.write_read("POWERUP3", None, 0x8d)      # PUSMState set to 13 (PAUSE_FOR_DLL_CONFIG), see lpgbt manual ch. 8.1

        if not self.efused:
            if self.master:
                for reg in self.config_file["power_up_registers_master"]:
                    self.write_read(reg, None, self.construct_reg_data(reg))
            else:
                for reg in self.config_file["power_up_registers_slave"]:
                    self.write_read(reg, None, self.construct_reg_data(reg))

        logger.info("Enable eclock output for GBCR%s", self.device_nr)
        self.write_read("EPCLK5CHNCNTRH", None, self.construct_reg_data("EPCLK5CHNCNTRH"))
        self.write_read("EPCLK5CHNCNTRL", None, self.construct_reg_data("EPCLK5CHNCNTRL"))

        if "524d" in self.optoboard_serial and self.master:      # set e-port receiver trackmode to setting stored in the config files (Optoboards with serial 524dxxxx are wrongly efused to fixed phase)
            logger.info("Changing wrongly efused trackmode bit to the config values..")
            for group in range(0,6):
                self.write_read("EPRX"+str(group)+"CONTROL", "EPRX"+str(group)+"TRACKMODE", self.config_file["lpgbt"]["EPRX"+str(group)+"CONTROL"]["EPRX"+str(group)+"TRACKMODE"][0])

        if self.master:
            self.configure_I2C_controller()

        if (self.optoboard_serial=="08000000") or ("524d" in self.optoboard_serial) or (self.optoboard_v==1) and self.master:
            logger.info("Releasing forced PUSM state..")
            self.write_read("POWERUP4", None, 0)
            self.write_read("POWERUP3", None, 0)

        check = self.check_PUSM_status()

        self.state = check["PUSM_state"]
        self.ready = check["PUSM_ready"]

        return self.ready

    def configure_I2C_controller(self):
        """Set the I2C controller settings of the lpGBT master and generate a reset pulse on it.

        Returns:
            configured (bool): True if lpGBT slave I2C controller configured
        """
        configured = False
        if not self.master:
            logger.warning("No need to configure I2C controller on lpgbt slave! Exiting..")
            return configured

        logger.info("Configuring I2C controller settings on I2C master %s and reset..", self.I2C_master)

        # configuring I2C settings on I2C master
        self.write_read("I2CM" + str(self.I2C_master) + "CONFIG", None, self.construct_reg_data("I2CM" + str(self.I2C_master) +"CONFIG"))
        configured = True
        # resetting I2C master
        logger.info("Reset the I2C master: generating a pulse 0->1->0 on bit RSTI2CM%s", self.I2C_master)

        self.write_read("RST0", "RSTI2CM" + str(self.I2C_master), 0)
        self.write_read("RST0", "RSTI2CM" + str(self.I2C_master), 1)
        self.write_read("RST0", "RSTI2CM" + str(self.I2C_master), 0)

        logger.info("I2C controller %s settings done", self.I2C_master)

        return configured

    def readAll(self):
        """Read out all registers of the lpGBT.

        Returns:

            all read values of the lpGBT in a dict, examples:

            'CHIPID0': 52,
            'CHIPID1: 77,
        """
        readout = {}

        for reg in self.lpgbt_reg_map.__dict__:
            if (reg != "Reg") and (reg[0] != "_"):
                readout[reg] = self.read(reg, None)

        return readout

    def bert(self, bertsource_g, bertsource_c, bertmeastime):
        """Perform a Bit Error Rate Test (BERT).

        Please consult the lpGBT manual ch. 14 or https://optoboard-system.docs.cern.ch/hardware/optoboard/lpgbt/#bit-error-rate-tests-bert for more information on the arguments and procedure.
        For low limits the BERT_error_count can be devided by 3 acc. to the lPGBT manual. The limit is calculated with upper limits from Statistics by R. J. Barlow (page 133).

        Args:
            bertsource_g (int): coarse source for BERT
                if EPRX link => bertsource_g = EPRX group + 1,
                if EPTX link => bertsource_g = EPTX group + 9
            bertsource_c (int): fine source for BERT
                if EPRX link => bertsource_c should be set to 6 (always channel 0) for 1.28 Gb/s,
                if EPTX link => bertsource_c should be 4 (channel 0) or 5 (channel 2)
            bertmeastime (int): controls the duration of the BERT, from 0 to 15 possible
                defaults to 11, which relates to approx. 3 seconds

        Returns:

            dict with the following keys and values:

            total_time (float): approx. time for BERT in seconds
            total_bits (int): amount of bits measured
            BERT_error_count (int): amount of errors detected
            BER_limit (float): limit calculated,
                if BERT_error_count < 11 => limit = poisson_upper_limits[BERT_error_count/3]/total_bits,
                if BERT_error_count > 10 => BERT_error_count + 1.96*(BERT_error_count)**(1/2) according to https://en.wikipedia.org/wiki/97.5th_percentile_point (Gaussian approximation)

        Raises:
            ValueError: BERT checker disabled
            ValueError: only channel 6 is used for 1.28 Gb/s uplinks
            NameError: downlink BERT on lpGBT slaves, downlinks only on lpGBT master
            ValueError: up-/downlink EC and downlink serializer not supported
            Exception: according to the lpGBT status check no data is arriving to the selected elink
        """
        poisson_upper_limits = [3.0, 4.74, 6.3, 7.75, 9.15, 10.51, 11.84, 13.15, 14.43, 15.71, 16.96]  # from Statistics by Barlow, p. 133

        logger.info("Starting BERT on %s..", self.device)

        logger.debug("BERT parameters - device: %s, bertsource_g: %s, bertsource_c: %s, bertmeastime: %s", self.device, bertsource_g, bertsource_c, bertmeastime)

        if bertsource_g == 0:
            logger.error("bertsource_g: %d, BERT checker is disabled", bertsource_g)
            raise ValueError("BERT checker is disabled!")

        if bertsource_g < 6 and not bertsource_c==6:
            logger.error("bertsource_c: %d, channel 1-3 are not available for 1.28 Gb/s elinks.", bertsource_c)
            raise ValueError("Wrong bertsource_c, channel 1-3 are not connected for 1.28 Gb/s elinks.")

        BERT_error_count = -1

        T_clock = 25*10**(-9)           # 40 MHz clock period [s]

        # bits/clock cycle for uplink data rates
        if bertsource_g <= 6:
            if bertsource_c==6:
                bits_per_clock_cycle = 32
            elif (bertsource_c in [4, 5]):
                bits_per_clock_cycle = 16
            else:
                bits_per_clock_cycle = 8

        # bits/clock cycle for downlink data rates
        elif (9 <= bertsource_g <= 12):
            if self.device != "lpgbt1":
                raise NameError("Can not do BERT for downlink on lpGBT slaves, downlinks only come from lpGBT1 (master)!")
            elif bertsource_c==6:
                bits_per_clock_cycle = 8
            elif (bertsource_c in [4, 5]):
                bits_per_clock_cycle = 4
            else:
                bits_per_clock_cycle = 2

        else:
            logger.error("bertsource_g: %d, Uplink/downlink data group EC and downlink deserializer frame not supported", bertsource_g)
            raise ValueError("Up/downlink EC or downlink deserializer frame not supported!")

        total_bits = 2**(5+2*bertmeastime)*bits_per_clock_cycle

        runtime_expected = 2**(5+2*bertmeastime)*T_clock        # amount of clock cycles * period of clock [s]

        logger.info("Expected runtime: %.2f s", runtime_expected)

        # start measurement
        t_start = time.time()   # start timer

        self.write_read("BERTSOURCE", None, (bertsource_g << 4) | bertsource_c)
        self.write_read("BERTCONFIG", "bertmeastime", bertmeastime)
        self.write_read("BERTCONFIG", "BERTSTART", 1)

        while True:
            status = bin(self.read("BERTStatus", None))[2:].zfill(8)

            logger.debug("while loop - status: %s", status)

            if int(status[7]):
                total_time = time.time()-t_start

                logger.info("BERT finished, saving results..")

                BERT_error_count = (self.read("BERTRESULT4", None) << 32) | (self.read("BERTRESULT3", None) << 24) | (self.read("BERTRESULT2", None) << 16) | (self.read("BERTRESULT1", None) << 8) | (self.read("BERTRESULT0", None))

                logger.info("Stopping measurement..")
                self.write_read("BERTCONFIG", None, 0)

                break

            elif int(status[5]):

                logger.error("BERT error flag (there was no data on the input group %s channel %s). Stopping measurement..", bertsource_g, bertsource_c)
                self.write_read("BERTCONFIG", None, 0)

                raise Exception("No data arrived at the pattern checker.")

        if BERT_error_count<11:
            mu = poisson_upper_limits[BERT_error_count]

        else:
            mu = BERT_error_count + 1.96*(BERT_error_count)**(1/2)	# we believe we can approximate it with this function.

        BER_limit = mu/total_bits

        if self.lpgbt_v:
            if BER_limit < 0.001:

                BERT_error_count = int(BERT_error_count/3)		# see docstring on why divided by 3

                if BERT_error_count<11:
                    mu = poisson_upper_limits[BERT_error_count]
                else:
                    mu = BERT_error_count + 1.96*(BERT_error_count)**(1/2)

                BER_limit = mu/total_bits

        logger.info(f"Total time: {total_time} s, total bits: {total_bits}, error bits: {BERT_error_count}, BER limit (95%% confidence interval) {BER_limit}")
        logger.info("BERT done!")

        return {"total_time": total_time, "total_bits": total_bits, "BERT_error_count": BERT_error_count, "BER_limit": BER_limit}

    def bert_internal(self, bertsource_g, bertsource_c, bertmeastime):
        """Perform an internal BERT by generating the PRBS7 with the lpGBT pattern generator.

        Args:
            bertsource_g (int): coarse source for BERT,
                if EPRX link => bertsource_g = EPRX group + 1,
                if EPTX link => bertsource_g = EPTX group + 9
            bertsource_c (int): fine source for BERT,
                if EPRX link => bertsource_c should be set to 6 (always channel 0) for 1.28 Gb/s,
                if EPTX link => bertsource_c should be 4 (channel 0) or 5 (channel 2)
            bertmeastime (int): controls the duration of the BERT, from 0 to 15 possible.
                defaults to 11, which relates to approx. 3 seconds

        Returns:

            dict with the following keys and values:

            total_time (float): approx. time for BERT in seconds
            total_bits (int): amount of bits measured
            BERT_error_count (int): amount of errors detected
            BER_limit (float): limit calculated,
                if BERT_error_count < 11 => limit = poisson_upper_limits[BERT_error_count/3]/total_bits,
                if BERT_error_count > 10 => BERT_error_count + 1.96*(BERT_error_count)**(1/2) according to https://en.wikipedia.org/wiki/97.5th_percentile_point (Gaussian approximation)

        Raises:
            ValueError: selected elink parameters outside possible scope
        """
        if bertsource_g<7 and not bertsource_g==0:
            logger.info("Performing an internal BERT on EPRX%s%s..", bertsource_g-1, bertsource_c)
            self.set_UL_elinks(bertsource_g-1, "PRBS7")
            results = self.bert(bertsource_g, bertsource_c, bertmeastime)
            self.set_UL_elinks(bertsource_g-1, "reset")

        elif bertsource_g>8 and bertsource_g<13:
            if bertsource_c==4: channel = 0
            else: channel = 2
            logger.info("Performing an internal BERT on EPTX%s%s..", bertsource_g-9, channel)
            self.set_DL_elinks(bertsource_g-9, "PRBS7")
            results = self.bert(bertsource_g, bertsource_c, bertmeastime)
            self.set_DL_elinks(bertsource_g-9, "reset")

        else:
            logger.error("Coarse source %s for BERT outside possible limits (EPRX BERT: 1-6, EPTX BERT: 9-12)", bertsource_g)
            raise ValueError("Coarse source for BERT outside possible limits!")

        return results

    def bert_by_elink(self, elink, bertmeastime=11):
        """Execute a BERT with short notation.

        Args:
            elink (str): short notation, provide EPTXGC or EPRXGC where G group and C channel
            bertmeastime (int, optional): controls the duration of the BERT, from 0 to 15 possible.

        Returns:

            dict with the following keys and values:

            total_time (float): approx. time for BERT in seconds
            total_bits (int): amount of bits measured
            BERT_error_count (int): amount of errors detected
            BER_limit (float): limit calculated,
                if BERT_error_count < 11 => limit = poisson_upper_limits[BERT_error_count/3]/total_bits,
                if BERT_error_count > 10 => BERT_error_count + 1.96*(BERT_error_count)**(1/2) according to https://en.wikipedia.org/wiki/97.5th_percentile_point (Gaussian approximation)

        Raises:
            ValueError: elink not possible
        """
        poss_elinks = ["EPRX00", "EPRX10", "EPRX20", "EPRX30", "EPRX40", "EPRX50",
                    "EPTX00", "EPTX02", "EPTX10", "EPTX12", "EPTX20", "EPTX22", "EPTX30", "EPTX32"]
        if not elink in poss_elinks:
            logger.error("Wrong elink string provided: %s, must be %s!", elink, poss_elinks)
            raise ValueError("Wrong elink string provided!")

        group = int(elink[4])
        channel = int(elink[5])
        rx = True
        if elink[2] == "T":
            rx = False

        if rx:
            bertsource_g = group + 1
            bertsource_c = 6

        else:
            bertsource_g = group + 9

            if channel == 0: bertsource_c = 4
            else: bertsource_c = 5

        return self.bert(bertsource_g, bertsource_c, bertmeastime)

    def get_lpgbt_info(self):
        """Get lpGBT CHIPID and USERID.

        The IDs can be unreliable due to efuse problems (see ch. 3.7 lpGBTv1 manual)

        Returns:

            dict with the following keys and values:

            CHIPID (hex): given by the lpGBT team
            USERID (hex): serial of Optoboard if efused by Bern
        """
        logger.info("Getting %s information..", self.device)

        CHIPID0 = self.read("CHIPID0", None)
        CHIPID1 = self.read("CHIPID1", None)
        CHIPID2 = self.read("CHIPID2", None)
        CHIPID3 = self.read("CHIPID3", None)

        CHIPID = CHIPID0 << 24 | CHIPID1 << 16 | CHIPID2 << 8 | CHIPID3

        logger.info("%s CHIPID: %s (%s)", self.device, CHIPID, hex(CHIPID))

        USERID0 = self.read("USERID0", None)
        USERID1 = self.read("USERID1", None)
        USERID2 = self.read("USERID2", None)
        USERID3 = self.read("USERID3", None)

        USERID = USERID0 << 24 | USERID1 << 16 | USERID2 << 8 | USERID3

        logger.info("%s USERID: %s (%s)", self.device, USERID, hex(USERID))

        return {"CHIPID" : hex(CHIPID), "USERID" : hex(USERID)}

    def check_PUSM_status(self):
        """Check lpGBTs power up state machine (PUSM) status.

        Returns:
            dict with the following keys and values:

            PUSM_state (int): state of the PUSM
            PUSM_ready (bool): True if lpGBT ready
        """
        state = self.read("PUSMSTATUS", "PUSMSTATE")
        ready = False

        PUSM2STR_V0 = ["ARESET", "RESET", "WAIT_VDD_STABLE", "WAIT_VDD_HIGHER_THAN_0V90",
            "FUSE_SAMPLING", "UPDATE_FROM_FUSES", "PAUSE_FOR_PLL_CONFIG", "WAIT_POWER_GOOD",
            "RESETOUT", "I2C_TRANS", "RESET_PLL", "WAIT_PLL_LOCK", "INIT_SCRAM",
            "PAUSE_FOR_DLL_CONFIG", "RESET_DLLS", "WAIT_DLL_LOCK",
            "RESET_LOGIC_USING_DLL", "WAIT_CHNS_LOCKED", "READY"]

        PUSM2STR_V1 = ["ARESET", "RESET", "WAIT_VDD_STABLE", "WAIT_VDD_HIGHER_THAN_0V90",
            "STATE_COPY_FUSES", "STATE_CALCULATE_CHECKSUM", "COPY_ROM", "PAUSE_FOR_PLL_CONFIG", "WAIT_POWER_GOOD",
            "RESET_PLL", "WAIT_PLL_LOCK", "INIT_SCRAM", "RESETOUT", "I2C_TRANS",
            "PAUSE_FOR_DLL_CONFIG", "RESET_DLLS", "WAIT_DLL_LOCK",
            "RESET_LOGIC_USING_DLL", "WAIT_CHNS_LOCKED", "READY"]

        if self.lpgbt_v:
            if state==19:
                logger.info("%s status is %s%s%s", self.device, LoggerFormat.green, PUSM2STR_V1[state], LoggerFormat.reset)
                ready = True
            else:
                logger.info("%s status is %s%s%s", self.device, LoggerFormat.red, PUSM2STR_V1[state], LoggerFormat.reset)

        else:
            if state==18:
                logger.info("%s status is %s%s%s", self.device, LoggerFormat.green, PUSM2STR_V0[state], LoggerFormat.reset)
                ready = True
            else:
                logger.info("%s status is %s%s%s", self.device, LoggerFormat.red, PUSM2STR_V0[state], LoggerFormat.reset)

        self.state = state

        return {"PUSM_state": state, "PUSM_ready" : ready}

    def check_lpgbt_config_pins(self):
        """Check lpGBTs hardware configuration.

        Checks for the transceiver mode and lock mode of lpGBT. For the Optoboard:
        - lpGBT master should be in transceiver 10.24 Gbps FEC12 mode and lock mode 1 (recover clock from data stream)
        - lpGBT slaves should be in transmitter 10.24 Gbps FEC12 mode and lock mode 0 (recover from clock)
        - BootConfig should be 0 (load from efuses) or 1 (load from lpGBT ROM)

        Note: these pins/modes can only be changed by changing resistors!

        Returns:

            dict with the following keys and values:

            BootConfig_ready (bool): True if 0 or 1
            Mode_ready (bool): True if correct mode
            LockMode_ready (bool): True if correct LockMode
        """
        logger.info("Checking %s hardware configuration..", self.device)

        MODE2STR = ["OFF", "TX 5.12Gbps FEC5", "RX 5.12Gbps FEC5", "TRX 5.12Gbps FEC5",
                "OFF", "TX 5.12Gbps FEC12", "RX 5.12Gbps FEC12", "TRX 5.12Gbps FEC12",
                "OFF", "TX 10.24Gbps FEC5", "RX 10.24Gbps FEC5", "TRX 10.24Gbps FEC5",
                "OFF", "TX 10.24Gbps FEC12", "RX 10.24Gbps FEC5", "TRX 10.24Gbps FEC12"]

        LPGBTMode = self.read("CONFIGPINS", "LPGBTMODE")
        LockMode = self.read("CONFIGPINS", "LOCKMODE")

        if self.lpgbt_v:
            BootConfig = self.read("CONFIGPINS", "BOOTCONFIG")
            if BootConfig<2:
                logger.info("%s BOOTCNF is %s%s%s", self.device, LoggerFormat.green, BootConfig, LoggerFormat.reset)
                BootConfig_ready = True
            else:
                logger.info("%s BOOTCNF is %s%s%s", self.device, LoggerFormat.red, BootConfig, LoggerFormat.reset)
                BootConfig_ready = False

        else:
            ConfigSelect = self.read("CONFIGPINS", "CONFIGSELECT")    # state of SC_I2C pin
            logger.info("%s SC_I2C is %s%s%s", self.device, LoggerFormat.red, ConfigSelect, LoggerFormat.reset)


        # lpGBT master MODE should be transceiver 10.24Gbps FEC12
        if self.master and LPGBTMode==15:
            logger.info("%s mode is %s%s%s", self.device, LoggerFormat.green, MODE2STR[LPGBTMode], LoggerFormat.reset)
            Mode_ready = True
        # lpGBT slave MODE should be transmitter 10.24Gbps FEC12, LOCKMODE 0 (external clock)
        elif not self.master and LPGBTMode==13:
            logger.info("%s mode is %s%s%s", self.device, LoggerFormat.green, MODE2STR[LPGBTMode], LoggerFormat.reset)
            Mode_ready = True
        else:
            logger.info("%s mode is %s%s%s", self.device, LoggerFormat.red, MODE2STR[LPGBTMode], LoggerFormat.reset)
            Mode_ready = False


        # lpGBT master LockMode should be 1 (reference-less, recover from data stream)
        if self.master and LockMode==1:
            logger.info("%s LockMode is %s%s%s", self.device, LoggerFormat.green, LockMode, LoggerFormat.reset)
            LockMode_ready = True
        # lpGBT slave LockMode should be 0 (external clock)
        elif not self.master and LockMode==0:
            logger.info("%s lock mode is %s%s%s", self.device, LoggerFormat.green, LockMode, LoggerFormat.reset)
            LockMode_ready = True
        else:
            logger.info("%s lock mode is %s%s%s", self.device, LoggerFormat.red, LockMode, LoggerFormat.reset)
            LockMode_ready = False

        return {"BootConfig_ready" : BootConfig_ready, "Mode_ready" : Mode_ready, "LockMode_ready" : LockMode_ready}

    def check_EPRX_locking(self):
        """Check lpGBTs elink uplink phase locking for all channels.

        Returns:

            Nested dictionaries with keys:
            `EPRX_group0`, `EPRX_group1`, `EPRX_group2`, `EPRX_group3`, `EPRX_group4`, `EPRX_group5`.
            Each has the following keys and values (example `EPRX_group0`):

            locking (bool): True if locked
            state (int): state of initialization state machine for EPRX00
            phase (int): current phase of lpGBT EPRX00
        """
        logger.info("Checking %s EPRX phase locking..", self.device)
        dict_EPRX_locking = {}

        STATE2STR = ["Reset", "Force down", "Confirm early state", "Free running state"]

        for G in range(0,6):

            G_str = str(G)
            dict_EPRX_locking["EPRX_group" + G_str] = {}

            eprxlocked = self.read("EPRX"+G_str+"LOCKED", None)

            phase_lock = eprxlocked >> 4	# get bits 7:4
            state = eprxlocked & 3		# get bits 1:0
            phase = self.read("EPRX"+G_str+"CURRENTPHASE10", "EPRX"+G_str+"CURRENTPHASE0")

            if int(bin(phase_lock)[-1]):
                dict_EPRX_locking["EPRX_group" + G_str]["locking"] = True
                dict_EPRX_locking["EPRX_group" + G_str]["state"] = state
                dict_EPRX_locking["EPRX_group" + G_str]["phase"] = phase
                logger.info("%s e-link %s (EPRX group %s channel 0) is %slocked%s with phase %s. State: %s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset, phase, STATE2STR[state])
            else:
                dict_EPRX_locking["EPRX_group" + G_str]["locking"] = False
                dict_EPRX_locking["EPRX_group" + G_str]["state"] = state
                dict_EPRX_locking["EPRX_group" + G_str]["phase"] = phase
                logger.info("%s e-link %s (EPRX group %s channel 0) is %snot locked%s with phase %s. State: %s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset, phase, STATE2STR[state])

        return dict_EPRX_locking

    def check_EPRX_DLL_locking(self):
        """Check lpGBTs elink uplink DLL locking for all channels.

        Returns:

            Nested dictionaries with keys:
            `EPRX_group0`, `EPRX_group1`, `EPRX_group2`, `EPRX_group3`, `EPRX_group4`, `EPRX_group5`.
            Each has the following keys and values (example `EPRX_group0`):

                DLL_lock (bool): True if locked
                DLL_state (int): state of lock filter state machine for EPRX00
                DLL_loss_count (int): loss of Lock counter value
        """
        logger.info("Checking %s EPRX DLL status..", self.device)
        dict_EPRX_DLL_locking = {}

        for G in range(0,6):

            G_str = str(G)
            dict_EPRX_DLL_locking["EPRX_group" + G_str] = {}

            eprxdllstatus = self.read("EPRX"+G_str+"DLLSTATUS", None)

            dll_lock = eprxdllstatus >> 7		# get bit 7
            state = (eprxdllstatus & 96) >> 5 		# get bits 6:5
            loss_counter = eprxdllstatus & 31		# get bits 4:0

            if dll_lock==1:
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_lock"] = True
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_state"] = state
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_loss_count"] = loss_counter
                logger.info("%s e-link %s (EPRX group %s channel 0) DLL is %slocked%s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset)
            else:
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_lock"] = False
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_state"] = state
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_loss_count"] = loss_counter
                logger.info("%s e-link %s (EPRX group %s channel 0) DLL is %snot locked%s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset)

        return dict_EPRX_DLL_locking

    def check_EPRX_status(self):
        """Check lpGBTs elink uplink status for all channels.

        Returns:

            Nested dictionaries with keys:
            `EPRX_group0`, `EPRX_group1`, `EPRX_group2`, `EPRX_group3`, `EPRX_group4`, `EPRX_group5`.
            Each has the following keys and values (example `EPRX_group0`):

            enable (bool): True if enabled
            data_rate (str): either 1.28 if 1.28 Gb/s or not 1.28
            polarity (str): inverted or not inverted
            selected_phase (int): selected phase for EPRX channel, this value can be different from the actual phase if channel is not in fixed phase mode!
        """
        TRACKMODE2STR = ["fixed phase", "initial training", "continuous phase tracking", "continuous phase tracking with initial training"]

        logger.info("Checking %s EPRX status..", self.device)
        dict_EPRX_status = {}

        for G in range(0,6):

            G_str = str(G)

            dict_EPRX_status["EPRX_group_" + G_str] = {}

            eprxcontrol = self.read("EPRX"+G_str+"CONTROL", None)
            EPRX_en = bool((eprxcontrol & 16) >>4)
            EPRX_data_rate = (eprxcontrol & 12) >> 2
            EPRX_track_mode = eprxcontrol & 3

            eprxchncntr = self.read("EPRX"+G_str+"0CHNCNTR", None)
            EPRX_pol = (eprxchncntr & 8) >> 3
            EPRX_phase = eprxchncntr >> 4

            dict_EPRX_status["EPRX_group_" + G_str]["enable"] = EPRX_en

            logger.info(EPRX_data_rate)

            if EPRX_en and not EPRX_data_rate==0:

                if EPRX_data_rate==3:
                    dict_EPRX_status["EPRX_group_" + G_str]["data_rate"] = "1.28"
                    logger.info("%s e-link %s (EPRX group %s channel 0) data rate is %s1.28 Gbps%s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset)
                else:
                    dict_EPRX_status["EPRX_group_" + G_str]["data_rate"] = "not 1.28"
                    logger.warning("%s e-link %s (EPRX group %s channel 0) data rate is %snot 1.28 Gbps%s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset)

                dict_EPRX_status["EPRX_group_" + G_str]["track_mode"] = TRACKMODE2STR[EPRX_track_mode]
                logger.info("%s e-link %s (EPRX group %s channel 0) track mode is %s%s%s", self.device, G_str, G_str, LoggerFormat.yellow, TRACKMODE2STR[EPRX_track_mode], LoggerFormat.reset)

                if EPRX_pol:
                    dict_EPRX_status["EPRX_group_" + G_str]["polarity"] = "inverted"
                    logger.info("%s e-link %s (EPRX group %s channel 0) is %sinverted%s", self.device, G_str, G_str, LoggerFormat.yellow, LoggerFormat.reset)
                else:
                    dict_EPRX_status["EPRX_group_" + G_str]["polarity"] = "not inverted"
                    logger.info("%s e-link %s (EPRX group %s channel 0) is %snot inverted%s", self.device, G_str, G_str, LoggerFormat.yellow, LoggerFormat.reset)

                dict_EPRX_status["EPRX_group_" + G_str]["selected_phase"] = EPRX_phase

            else:
                logger.info("%s e-link %s (EPRX group %s channel 0) is %snot enabled%s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset)

        return dict_EPRX_status

    def bypass_ul(self):
        """Set the lpGBTs 10.24 Gb/s uplink to output a 40 MHz clock and bypass the interleaver, scrambler and FEC encoder.

        Changes the data link from the lpGBT and through the fibres to a 40 MHz clock. Useful for debugging as the clock signal can be ssen on an oscilloscope with an optical to electrical converter.

        Warning: breaks the communication if one sets it on the lpGBT master - only a power cycle will restore communication!
        """
        clock_40MHz = 8

        if self.master:
            logger.warning("You are changing the interleaved, scrambled, forward encoded 10.24 Gbps uplink of the lpgbt master to a raw 40 MHz clock pattern! This breaks communication via IC and you need to power-cycle the Optoboard to re-establish!")

        logger.info("Configuring UL data path bypassing..")

        self.write_read("DATAPATH", None, 7)

        logger.warning("%s 10.24 Gbps uplink bypassing interleaver, scrambler and FEC encoder", self.device)

        logger.warning("Changing UL serializer pattern to 40 MHz clock!")
        self.write_read("ULDATASOURCE0", "ULSERTESTPATTERN", clock_40MHz)

    def set_UL_elinks(self, group, data_source):
        """Set data source of EPRX group.

        The output of any EPRX group can be set to a known pattern i.e. PRBS7. The pattern can be analysed on the FELIX side if one extracts the elink data on FELIX.

        Note: for the CONST_PATTERN option, registers DPDataPattern0, DPDataPattern1, DPDataPattern2, DPDataPattern3 need to be set with the wanted pattern.

        Args:
            group (int): EPRX group (elink number)
            data_source (str): data source of EPRX group, possibilities:
                "EPORTRX_DATA", "PRBS7", "BIN_CNTR_UP", "BIN_CNTR_DOWN", "CONST_PATTERN", "CONST_PATTERN_INV", "DLDATA_LOOPBACK"

        Returns:
            read (int): data source index read back

        Raises:
            ValueError: pattern to select not possible
            ValueError: elink group outside scope
        """
        patterns = ["EPORTRX_DATA", "PRBS7", "BIN_CNTR_UP", "BIN_CNTR_DOWN", "CONST_PATTERN", "CONST_PATTERN_INV", "DLDATA_LOOPBACK"]

        if data_source == "reset":
            data_source = "EPORTRX_DATA"

        if data_source not in patterns:
            logger.error("Can not set %s for uplinks, not in %s", data_source, patterns)
            raise ValueError("Can not set uplink generator to chosen pattern for uplinks!")

        data_source_index = patterns.index(data_source)

        logger.info("Configure %s EPRX%s0 uplink data path to %s..", self.device, group, data_source)
        read = 0

        ulgdatasource = "ULG"+str(group)+"DATASOURCE"
        if group>= 0 and group<2:
            read = self.write_read("ULDATASOURCE1", ulgdatasource, data_source_index)
        elif group>1 and group<4:
            read = self.write_read("ULDATASOURCE2", ulgdatasource, data_source_index)
        elif group>3 and group<6:
            read = self.write_read("ULDATASOURCE3", ulgdatasource, data_source_index)
        else:
            logger.error("Group %s not allowed, 0 to 5 possible", group)
            raise ValueError("Wrong group input for setting UL elinks generator.")

        if data_source != "EPORTRX_DATA":
            logger.warning("Data path disabled, sending %s test pattern on EPRX%s0!", data_source, group)
        else:
            logger.info("%s EPRX%s0 elinks set to %sdata path%s!", self.device, group, LoggerFormat.green, LoggerFormat.reset)

        return read

    def set_DL_elinks(self, group, data_source):
        """Check lpGBTs elink uplink status for all channels.

        The output of any EPTX group can be set to a known pattern i.e. PRBS7. The pattern can be analysed on the module side.

        Note: for the CONST_PATTERN option, registers DPDataPattern0, DPDataPattern1, DPDataPattern2, DPDataPattern3 need to be set with the wanted pattern.

        Args:
            group (int): EPTX group (elink number), possible: [0, 1, 2, 3]
            data_source (str): data source of EPTX group, possibilities:
                "LINK_DATA", "PRBS7", "BIN_CNTR_UP, "CONST_PATTERN"

        Returns:
            read (int): data source index read back

        Raises:
            ValueError: pattern to select not possible
            ValueError: elink group outside scope
        """
        patterns = ["LINK_DATA", "PRBS7", "BIN_CNTR_UP", "CONST_PATTERN"]

        if data_source == "reset":
            data_source = "LINK_DATA"

        if data_source not in patterns:
            logger.error("Can not set %s for downlinks, not in %s", data_source, patterns)
            raise ValueError("Can not set downlink generator to chosen pattern for downlinks!")

        if group > 3:
            logger.error("Group %s not allowed, 0 to 5 possible", group)
            raise ValueError("Wrong group input for setting UL elinks generator.")

        data_source_index = patterns.index(data_source)

        logger.info("Configure %s EPTX%s0 and EPTX%s2 downlink data path to %s..", self.device, group, group, data_source)

        read = self.write_read("ULDATASOURCE5", "DLG"+str(group)+"DATASOURCE", data_source_index)

        if data_source != "LINK_DATA":
            logger.warning("Data path disabled, sending %s test pattern on EPTX%s0 and EPTX%s2 downlink elinks!", data_source, group, group)
        else:
            logger.info("%s EPTX%s0 and EPTX%s2 elinks set to %sdata path%s!", self.device, group, group, LoggerFormat.green, LoggerFormat.reset)

        return read

    def read_adc(self, adcchannel):
        """Read the ADC channel of the lpGBT.

        Time-out set to 5 s.

        Args:
            adcchannel (int): ADC channel to measure, from 0 to 15

        Returns:
            adcval (int): ADC value of the channel

        Raises:
            ValueError: wrong ADC channel input
            TimeoutError: ADC conversion takes to long
        """
        if adcchannel > 15 or adcchannel < 0:
            logger.error("ADC channel %s not available, 0 to 15 possible.", adcchannel)
            raise ValueError("ADC channel not available!")

        # Configure input multiplexers to measure ADC in single ended modePins
        ADCInPSelect = adcchannel
        ADCInNSelect = 15			# always VREF/2 or 15 because only single ended and internal mode on Opto V1
        self.write_read("ADCSELECT", None, (ADCInPSelect << 4) | ADCInNSelect) # ADCSelect

        # enable ADC core and set gain of the differential amplifier
        ADCEnable = 1
        ADCGainSelect = 0			# 0: x2, 1: x8, 2: x16, 3: x32
        self.write_read("ADCCONFIG", None, (ADCEnable << 2) | ADCGainSelect) # Enable ADC and select gain (ADCConfig)

        VREFEnable = 1
        VREFTune = 56		# from fig 18.3 of lpGBTv0 manual

        # Enable internal voltage reference
        self.write_read("VREFCNTR", None, (VREFEnable << 7) | VREFTune) # Enable internal voltage reference (VREFCNTR), VREFEnable (1), VREFTune (Tuning 0x38)

        # Wait until voltage reference is stable
        sleep(0.01)

        # Start ADC conversion, enable ADC core and set gain of the differential amplifier
        ADCConvert = 1
        self.write_read("ADCCONFIG", None, (ADCConvert << 7) | (ADCEnable << 2) | ADCGainSelect) # Start conversion (ADCConfig)

        timeout_time = time.time() + 5

        while True:

            adc_done = self.read("ADCSTATUSH", "ADCDone")

            if adc_done:

                adc_val_H = self.read("ADCSTATUSH", None) & 3		# take only bits 1:0 from the register ADCStatusH (ADCValue[9:8])
                adc_val_L = self.read("ADCSTATUSL", None)		# ADCValue[7:0]

                adc_val = (adc_val_H << 8) | adc_val_L		# calculate ADCValue[9:0]

                logger.info("ADC ch. %s value: %s", adcchannel, adc_val)
                break

            else:
                logger.info("ADCBusy - ADC core is performing conversion")

                if time.time() > timeout_time:
                    logger.error("Timeout while waiting for reading ADC channel, waited for 5 seconds")
                    raise TimeoutError("Timeout while waiting for reading ADC channel")

            sleep(0.1)

        return adc_val

    def phase_training(self, group):
        """Switches the mode of the EPRX group to Initial Training and does a phase training.

        See ch. 7.6.2 lpGBTv1 manual for more information.

        Args:
            group (int): Uplink EPRX data group to train phase, allowed [0, 1, 2, 3, 4, 5]

        Returns:
            locked (bool): True if channel locked after training
        """
        G_str = str(group)
        locked = False

        self.write_read("EPRX"+G_str+"CONTROL", "EPRX"+G_str+"TRACKMODE", 1)

        if group in [0,1]:
            self.write_read("EPRXTRAIN10", "EPRX"+G_str+"TRAIN", 1)
            self.write_read("EPRXTRAIN10", "EPRX"+G_str+"TRAIN", 0)
        elif group in [2,3]:
            self.write_read("EPRXTRAIN32", "EPRX"+G_str+"TRAIN", 1)
            self.write_read("EPRXTRAIN32", "EPRX"+G_str+"TRAIN", 0)
        elif group in [4,5]:
            self.write_read("EPRXTRAIN54", "EPRX"+G_str+"TRAIN", 1)
            self.write_read("EPRXTRAIN54", "EPRX"+G_str+"TRAIN", 0)
        else:
            logger.error("%s EPRX group %s can not be trained because it does not exist", self.device, G_str)
            return

        phase_lock = self.read("EPRX"+G_str+"LOCKED", "EPRX"+G_str+"CHNLOCKED")
        phase = self.read("EPRX"+G_str+"CURRENTPHASE10", "EPRX"+G_str+"CURRENTPHASE0")

        if int(bin(phase_lock)[-1]):
            logger.info("%s e-link %s (EPRX group %s channel 0) is %slocked%s with phase %s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset, phase)
            locked = True
        else:
            logger.info("%s e-link %s (EPRX group %s channel 0) is %snot locked%s, set phase: %s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset, phase)

        return locked

    def fuses_read_bank(self, address, timeout=0.01):
        """Read a single bank of eFuses.

        One bank is 32 bits and contains 4 registers.

        Args:
            address (int): first register address to read (multiple of 4)
            timeout (float): timeout for read completion in seconds (default 0.01)

        Returns:
            value (int): value of the fuse bank

        Raises:
            ValueError: fuse bank address wrong
            TimeoutError: reading efuses takes too long
        """
        if address%4 != 0 or address > self.lpgbt_reg_map.I2CM0CONFIG.address:
            logger.error("Incorrect address for burn bank! (address = 0x%s)", address)
            raise ValueError("Incorrect address for burn bank!")

        _, _, address_high, address_low = self.u32_to_bytes(address)
        self.write_read("FUSEBLOWADDH", None, address_high)
        self.write_read("FUSEBLOWADDL", None, address_low)
        self.write_read("FUSECONTROL", "FUSEREAD", 1)

        timeout_time = time.time() + timeout

        while True:
            status = self.read("FUSESTATUS", "FUSEDATAVALID")

            if status:
                fuse0 = self.read("FUSEVALUESA", None)
                fuse1 = self.read("FUSEVALUESB", None)
                fuse2 = self.read("FUSEVALUESC", None)
                fuse3 = self.read("FUSEVALUESD", None)

                value = fuse0 | fuse1 << 8 | fuse2 << 16 | fuse3 << 24
                self.write_read("FUSECONTROL", None, 0)
                break

            if time.time() > timeout_time:
                self.write_read("FUSECONTROL", None,  0)
                logger.error("Timeout while waiting for reading fuses")
                raise TimeoutError("Timeout while waiting for reading fuses")

        return value

    def read_fuses(self):
        """Read all eFuse banks.

        Goes from 0x000 to 0x0ff (register CRC3)

        Returns:
            fuse_values (dict): dictionary with bank address as key and tuple as values.
        """
        if not self.ready:
            logger.warning("lpGBT not ready, reading efuses should be done if the chip is in ready mode! Reading efuses anyway..")

        fuse_values = {}

        for adr in range(0, self.lpgbt_reg_map.I2CM0CONFIG.address, 4):
            fuse_values[adr] = self.u32_to_bytes(self.fuses_read_bank(adr))

        return fuse_values

    def u32_to_bytes(self, val):
        """Convert 32 bits to 4 bytes.

        Args:
            val (int): value to convert to bytes

        Returns:
            byte3, byte2, byte1, byte0 (tuple): bytes of value
        """
        byte3 = (val >> 24) & 0xFF
        byte2 = (val >> 16) & 0xFF
        byte1 = (val >> 8) & 0xFF
        byte0 = (val >> 0) & 0xFF

        return (byte3, byte2, byte1, byte0)

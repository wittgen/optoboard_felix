# pylint: disable=line-too-long, invalid-name, import-error
"""Optoboard module."""

import json
from collections import OrderedDict
import logging
import logging.handlers
from datetime import datetime

try:
    import matplotlib.pyplot as plt
    import matplotlib.colors as colors
    import matplotlib.cm as cm
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of matplotlib! Some function might not work properly!")
try:
    import numpy as np
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of numpy! Some function might not work properly!")

import time
import subprocess

from driver.log import logger, ch

from driver.Lpgbt import Lpgbt
from driver.Gbcr import Gbcr
from driver.Vtrx import Vtrx

now = datetime.now()
date_time = now.strftime("%Y-%m-%d_%H-%M")

def load_custom_config(path):
    """Load config from file.

    Args:
        path (str): path to config

    Returns:
        config_file (dict): contents of file

    Raises:
        Exception: config file does not exist
    """
    try:
        logger.info('Loading custom config from %s', path)
        with open(path) as f:
            config_file = json.load(f)
    except:
        raise Exception("Unavailable configuration file!")

    return config_file

def load_default_config(components, vtrx_v):
    """Load default config from file.

    Args:
        components (dict): contains Optoboard chip versions
        vtrx_v (str): connected VTRx+ version 

    Returns:
        config_file (dict): contents of file

    Raises:
        Exception: config file does not exist
    """
    path = "configs/optoboard" + str(components["optoboard_v"]) + "_lpgbtv" + str(components["lpgbt_v"]) + "_gbcr" + str(components["gbcr_v"]) + "_vtrxv" + vtrx_v.replace(".", "_") + "_default.json"

    try:
        logger.info('Loading default config from %s', path)
        with open(path) as f:
            config_file = json.load(f)
    except:
        raise Exception("Unavailable configuration file!")

    return config_file


class Optoboard:
    """Class for handling the chips on an Optoboard."""

    def __init__(self, config_file, optoboard_serial, vtrx_v, components_opto, Communication_wrapper, debug=False):
        """Initialise the Optoboard object.

        Initialisation sets the debug level of the logger, not fully optimized yet.
        Args:
            config_file (dict): dictionary with list of registers to be configured and their values
            optoboard_serial (str): serial number of the optoboard
            vtrx_v (str): VTRx+ quad laser driver version
            components_opto (dict): ordered dictionary of the components.py dictionary
            Communication_wrapper (class): low level access to FELIX access
            debug (bool, optional): True for debug mode on, Flase per default

        Attributes:
            config_file (dict): dictionary with list of registers to be configured and their values
            optoboard_serial (str): serial number of the optoboard
            debug (bool, optional): True for debug mode on, Flase per default
            component (dict): ordered dictionary of the components.py dictionary
            lpgbt_v (int): lpGBT version
            gbcr_v (int): GBCR version
            lpgbt_master_address (hex): I2C address of the lpGBT
            I2C_master (int): I2C controller to use (can be 0, 1, 2)
            efused (int): 1 if Optoboard is efused
            activeLpgbt (int): mounted lpGBTs on Optoboard
            activeGbcr (int): mounted GBCRs on Optoboard
            devices (dict): ordered dictionary of active devices
            lpgbt* (class): lpGBT* object where * can be 1, 2, 3, 4 according to devices mounted
            gbcr* (class): GBCR* object where * can be 1, 2, 3, 4 according to devices mounted
            vtrx (class): VTRx+ object
        """
        self.config_file = config_file
        self.optoboard_serial = optoboard_serial

        self.debug = debug
        if debug:
            logger.setLevel(logging.DEBUG)
            ch.setLevel(logging.DEBUG)
            logger.info("Set logger level to DEBUG")
        else:
            logger.setLevel(logging.INFO)
            ch.setLevel(logging.INFO)     
            logger.info("Logger level at INFO")

        self.component = OrderedDict(components_opto)
        self.lpgbt_v = self.component["lpgbt_v"]
        self.gbcr_v = self.component["gbcr_v"]
        self.optoboard_v = self.component["optoboard_v"]
        self.lpgbt_master_addr = self.component["lpgbt_master_addr"]
        self.I2C_master = self.component["I2C_master"]
        self.efused = self.component["efused"]

        self.activeLpgbt = (self.component["lpgbt1"] << 3) | (self.component["lpgbt2"] << 2) | (self.component["lpgbt3"] << 1) | (self.component["lpgbt4"])
        self.activeGbcr = (self.component["gbcr1"] << 3) | (self.component["gbcr2"] << 2) | (self.component["gbcr3"] << 1) | (self.component["gbcr4"])

        self.devices = OrderedDict()

        logger.info("component: %s", self.component)

        for device, mounted in self.component.items():
            if mounted:
                if device=="lpgbt1":
                    self.lpgbt1 = Lpgbt(Communication_wrapper, "lpgbt1", self.lpgbt_master_addr, True, self.I2C_master, config_file.copy(), self.optoboard_serial, self.optoboard_v, self.lpgbt_v, self.efused)
                    self.devices["lpgbt1"] = self.lpgbt1
                elif device=="lpgbt2":
                    self.lpgbt2 = Lpgbt(Communication_wrapper, "lpgbt2", 0x75, False, self.I2C_master, config_file.copy(), self.optoboard_serial, self.optoboard_v, self.lpgbt_v, self.efused)
                    self.devices["lpgbt2"] = self.lpgbt2
                elif device=="lpgbt3":
                    self.lpgbt3 = Lpgbt(Communication_wrapper, "lpgbt3", 0x76, False, self.I2C_master, config_file.copy(), self.optoboard_serial, self.optoboard_v, self.lpgbt_v, self.efused)
                    self.devices["lpgbt3"] = self.lpgbt3
                elif device=="lpgbt4":
                    self.lpgbt4 = Lpgbt(Communication_wrapper, "lpgbt4", 0x77, False, self.I2C_master, config_file.copy(), self.optoboard_serial, self.optoboard_v, self.lpgbt_v, self.efused)
                    self.devices["lpgbt4"] = self.lpgbt4

                elif device=="gbcr1":
                    self.gbcr1 = Gbcr(Communication_wrapper, "gbcr1", 0x20, False, self.I2C_master, config_file.copy(), self.gbcr_v)
                    self.devices["gbcr1"] = self.gbcr1
                elif device=="gbcr2":
                    self.gbcr2 = Gbcr(Communication_wrapper, "gbcr2", 0x21, False, self.I2C_master, config_file.copy(), self.gbcr_v)
                    self.devices["gbcr2"] = self.gbcr2
                elif device=="gbcr3":
                    self.gbcr3 = Gbcr(Communication_wrapper, "gbcr3", 0x22, False, self.I2C_master, config_file.copy(), self.gbcr_v)
                    self.devices["gbcr3"] = self.gbcr3
                elif device=="gbcr4":
                    self.gbcr4 = Gbcr(Communication_wrapper, "gbcr4", 0x23, False, self.I2C_master, config_file.copy(), self.gbcr_v)
                    self.devices["gbcr4"] = self.gbcr4

        self.vtrx = Vtrx(Communication_wrapper, "vtrx", 0x50, False, self.I2C_master, config_file.copy(), vtrx_v, int('{:04b}'.format(self.activeLpgbt)[::-1],2))
        self.devices["vtrx"] = self.vtrx

        logger.info("Optoboard object initialised!")
        logger.debug("devices initialised: %s", self.devices)


    def configure(self, customactivelpGBT=None, customactiveGBCR=None):
        """Configure the Optoboard according to the configuration provided in the initialization phase.

        Args:
            customactivelpGBT (str): string of 4 characters denoting which lpGBTs are to be configured
            customactiveGBCR (str):	string of 4 characters denoting which GBCRs are to be configured
        """
        configure_devices = list(self.devices.items())[0:1] + list(self.devices.items())[:0:-1]
        for device, obj in configure_devices:
            if "lpgbt" in device:
                if (customactivelpGBT is None) or (customactivelpGBT[int(device[-1])-1] == "1"):
                    logger.info("Configuring %s..", device)
                    obj.configure()

                    if device == "lpgbt1":
                        if self.component["lpgbt2"]:
                            obj.write_read("EPCLK2CHNCNTRH", None, obj.construct_reg_data("EPCLK2CHNCNTRH"))
                            obj.write_read("EPCLK2CHNCNTRL", None, obj.construct_reg_data("EPCLK2CHNCNTRL"))
                            logger.info("Enable eclock output for lpGBT2")

                        if self.component["lpgbt3"]:
                            obj.write_read("EPCLK26CHNCNTRH", None, obj.construct_reg_data("EPCLK26CHNCNTRH"))
                            obj.write_read("EPCLK26CHNCNTRL", None, obj.construct_reg_data("EPCLK26CHNCNTRL"))
                            logger.info("Enable eclock output for lpGBT3")

                        if self.component["lpgbt4"]:
                            obj.write_read("EPCLK19CHNCNTRH", None, obj.construct_reg_data("EPCLK19CHNCNTRH"))
                            obj.write_read("EPCLK19CHNCNTRL", None, obj.construct_reg_data("EPCLK19CHNCNTRL"))
                            logger.info("Enable eclock output for lpGBT4")

            elif "gbcr" in device:
                if (customactiveGBCR is None) or (customactiveGBCR[int(device[-1])-1] == "1"):
                    logger.info("Configuring %s..", device)
                    obj.configure()
            elif "vtrx" in device:
                logger.info("Configuring %s..", device)
                obj.configure()

    def opto_doc(self):
        """Check the status of the different devices on the Optoboard.

        Checks lpGBT info, PUSM status, lpGBT configuration pins, EPRX setting and locking status.

        Returns:
            opto_doc_dic (dict): status dictionary with devices as keys
        """
        opto_doc_dic = {}
        for device, obj in self.devices.items():
            logger.info("checking %s..", device)

            if device=="lpgbt1":
                opto_doc_dic[device] = {
                    **obj.get_lpgbt_info(),
                    **obj.check_PUSM_status(),
                    **obj.check_lpgbt_config_pins(),
                    **obj.check_EPRX_status(),
                    **obj.check_EPRX_locking()
                    }


            elif device in ["lpgbt2", "lpgbt3", "lpgbt4"]:
                opto_doc_dic[device] = {
                    **obj.get_lpgbt_info(),
                    **obj.check_PUSM_status(),
                    **obj.check_lpgbt_config_pins(),
                    **obj.check_EPRX_status(),
                    **obj.check_EPRX_locking()
                    }

            else:
                logger.debug("other devices to come..")

        logger.info("Optoboard checked!")

        return opto_doc_dic

    def reset_lpgbts(self):
        """Resets the lpGBTs. If any of the lpGBTs is not configurable after reset, power-cycle the Optoboard.

        See ch. 15.1.4 lpGBTv1 manual for more details 
        """
        logger.info("Checking if lpGBT master is READY")
        if self.lpgbt1.check_PUSM_status()["PUSM_ready"]:

            logger.warning("Resetting the lpGBTs")
            logger.warning("Writing 1 in ChipConfig_highSpeedDataInInvert")
            self.lpgbt1.comm_wrapper("CHIPCONFIG",64, False)

        else: 
            logger.info("lpGBT master is not in READY state: resetting is not possible!")


    def bertScan(self, lpgbt_name, gbcr_name, bertsource_g, bertsource_c, bertmeastime, filename="bertScan_result", plot=False, HFmin=0, HFmax=16, MFmin=0, MFmax=16, jump=1):
        """Perform a 2D scan of the results of BERT as a function of the parameters of the equalizer of the GBCR.

        Args:
            lpgbt_name (str): lpGBT to initialize/scan, options: lpgbt1, lpgbt2, lpgbt3, lpgbt4
            gbcr_name (str): GBCR to initialize/scan, options: gbcr1, gbcr2, gbcr3, gbcr4
            bertsource_g (int): coarse source for BERT
            bertsource_c (int): fine source for BERT
            bertmeastime (int): controls the duration of the BERT, from 0 to 15 possible.
            filename (str, optional): name of the file where the results are saved (default "bertScan_result"). Set to None for no result file.
            plot (bool, optional): create plot of scanned equalizer registers (default False)
            HFmin (int, optional): CTLE high frequency lower bound (default 0)
            HFmax (int, optional): CTLE high frequency upper bound (default 16)
            MFmin (int, optional): CTLE mid frequency lower bound (default 0)
            MFmax (int, optional): CTLE mid frequency upper bound (default 16)
            jump (int, optional): level of detail of the scan of the GBCR parameters (default 1)
        
        Raises:
            Exception: Initialisation failes
            ValueError: lpGBT and GBCR number not identical
            Exception: GBCR version not supported
        """
        try:
            lpgbt_obj = getattr(self, lpgbt_name)
            gbcr_obj = getattr(self, gbcr_name)
        except:
            logger.error("Initialisation of the lpGBT or GBCR object failed!")
            raise Exception("The initialisation of the chips for the BERT scan failed.")

        if lpgbt_name[-1] != gbcr_name[-1]:
            logger.error("lpGBT# should be the same as GBCR#, input: %s %s", lpgbt_name, gbcr_name)
            raise ValueError("lpGBT and GBCR number need to be identical")

        gbcrChannel = str(6 - (bertsource_g - 1))
        data = []

        if gbcr_obj.gbcr_v == 2:

            for hfSetting in range(HFmin,HFmax,jump):
                for mfSetting in range(MFmin,MFmax,jump):
                    logger.info("GBCR CTLEHFSR %s CTLEMFSR %s", hfSetting, mfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEHFSR", hfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEMFSR", mfSetting)
                    output = lpgbt_obj.bert(bertsource_g, bertsource_c, bertmeastime)

                    if filename is not None:
                        with open(filename + ".txt", "a") as f:
                            f.write(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": output}))
                            f.write("\n")
                    data.append(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": output}))

        elif gbcr_obj.gbcr_v == 3:

            for hfSetting in range(HFmin,HFmax,jump):
                for mfSetting in range(MFmin,MFmax,jump):
                    logger.info("GBCR CTLEHFSR %s CTLEMFSR %s", hfSetting, mfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEHF1SR", hfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEHF2SR", hfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK2", "CH" + gbcrChannel + "CTLEHF3SR", hfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK2", "CH" + gbcrChannel + "CTLEMFSR", mfSetting)
                    output = lpgbt_obj.bert(bertsource_g, bertsource_c, bertmeastime)

                    if filename is not None:
                        with open(filename + ".txt", "a") as f:
                            f.write(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": output}))
                            f.write("\n")
                    data.append(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": output}))

        else:
            raise Exception("Wrong GBCR version!")

        if plot:
            logger.info("Plotting the results of the BERT scan")
            self.plotHeatMap(filename, data)


    def softErrorCounter(self, optical_link, egroup, meastime):
        """The number of soft errors is monitored by checking the associated felix register.
        `flx-reset SOFT_RESET` command is called after each readout of the felix register therefore this method CANNOT BE CALLED IF FELIXCORE IS RUNNING!

        Args:
            optical_link (str): optical link to monitor, example: "00" for felix channel 0
            egroup (int): lpGBT egroup to monitor
            meastime (int): measuring time in seconds
        Returns:
            List with the measured errors, to be summed to have the total amount of errors
        """

        logger.warning("Turn off felixcore")
        aligned_link = "ALIGNED_" + optical_link
        felix_link = "LINK_" + optical_link + "_ERRORS_EGROUP" + str(egroup)
        start_time = time.time()
        counter = []
        subprocess.Popen(["flx-reset", "SOFT_RESET"], stdout=subprocess.PIPE)
        aligned = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE)
        aligned = subprocess.Popen(["grep", aligned_link], stdin=aligned.stdout, stdout=subprocess.PIPE).stdout.readlines()[0].decode().split()[4][-(egroup+1)]

        while (time.time()-start_time < meastime):

            if (aligned!="1"): 
                logger.info("Lost decoding alignment! Skipping to next setting!")
                counter.append(-1)
                break

            try:
                output = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE)
                output = subprocess.Popen(["grep", felix_link], stdin=output.stdout, stdout=subprocess.PIPE).stdout.readlines()[0].decode().split()[4]
            except:
                logger.error("Readout of FELIX register failed!") 
                raise Exception("Readout of FELIX register failed!")
            if (output!="0x0"):
                counter.append(int(output,16))
            #if (output=="0xf"):
                subprocess.Popen(["flx-reset", "SOFT_RESET"], stdout=subprocess.PIPE)
            time.sleep(2)

        return counter


    def softErrorScan(self, optical_link, egroup, gbcr_name, meastime, filename="softErrorScan_result", plot=False, HFmin=0, HFmax=16, MFmin=0, MFmax=16, jump=1):
        """Perform a 2D soft error scan as a function of the parameters of the equalizer of the GBCR.

        Args:
            optical_link (str): optical link to monitor
            egroup (int): egroup to monitor
            meastime (int): measuring time in seconds
            filename (str, optional): name of the file where the results are saved (default "bertScan_result"). Set to None for no result file.
            plot (bool, optional): create plot of scanned equalizer registers (default False)
            HFmin (int, optional): CTLE high frequency lower bound (default 0)
            HFmax (int, optional): CTLE high frequency upper bound (default 16)
            MFmin (int, optional): CTLE mid frequency lower bound (default 0)
            MFmax (int, optional): CTLE mid frequency upper bound (default 16)
            jump (int, optional): level of detail of the scan of the GBCR parameters (default 1)
        
        Raises:
            Exception: Initialisation failes
            Exception: FELIX register readout fails
            Exception: GBCR version not supported
        """
        try:
            gbcr_obj = getattr(self, gbcr_name)
        except:
            logger.error("Initialisation of the GBCR object failed!") 
            raise Exception("The initialisation of the chips for the soft error scan failed.")

        gbcrChannel = str(6 - egroup )
        data = []

        for hfSetting in range(HFmin,HFmax,jump):
            for mfSetting in range(MFmin,MFmax,jump):

                if gbcr_obj.gbcr_v == 2:
                    logger.info("GBCR CTLEHFSR %s CTLEMFSR %s", hfSetting, mfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEHFSR", hfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEMFSR", mfSetting)

                elif gbcr_obj.gbcr_v == 3:  
                    logger.info("GBCR CTLEHFSR %s CTLEMFSR %s", hfSetting, mfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEHF1SR", hfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEHF2SR", hfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK2", "CH" + gbcrChannel + "CTLEHF3SR", hfSetting)
                    gbcr_obj.write_read("CH" + gbcrChannel + "UPLINK2", "CH" + gbcrChannel + "CTLEMFSR", mfSetting)
               
                else:
                    raise Exception("Wrong GBCR version!")

                counter = self.softErrorCounter(optical_link, egroup, meastime)

                if filename is not None:
                    with open(filename + ".txt", "a") as f:
                        f.write(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": counter}))
                        f.write("\n")
                data.append(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": counter}))

        if plot:
            logger.info("Plotting the results of the BERT scan")
            self.plotHeatMap(filename, data)


    @staticmethod
    def plotHeatMap(filename, data):
        """Plot heat map of the BERT equalizer scan results.

        Args:
            filename (str): name of the plot file generated
            data (list): data from the scan
        """
        xPoints = np.array([json.loads(x)["CTLEHFSR"] for x in data])
        xPoints = np.sort(np.unique(xPoints))
        yPoints = np.array([json.loads(x)["CTLEMFSR"] for x in data])
        yPoints = np.sort(np.unique(yPoints))
        heatmap = np.array([json.loads(x)["results"]["BER_limit"] for x in data])
        heatmap = np.transpose(np.reshape(heatmap, (len(xPoints), len(yPoints))))

        plt.figure(figsize=(8,6))
        plt.imshow(heatmap, cmap = "hot", norm=colors.LogNorm(), origin="lower", extent=[xPoints[0]-0.5, xPoints[-1] + 0.5, yPoints[0]-0.5, yPoints[-1] + 0.5])
        plt.xticks(xPoints)
        plt.yticks(yPoints)
        plt.ylabel("CTLEMFSR")
        plt.xlabel("CTLEHFSR")
        plt.title("GBCR equalizer scan of the BER limit")
        plt.colorbar()
        plt.clim(10**(-12), 0.5)
        plotname = filename.split(".")[0] + "_plot.png"
        plt.savefig(plotname, format="png")


    @staticmethod
    def plotSoftErrorHeatMap(filename, data, time):
        """Plot heat map of the BERT equalizer scan results.

        Args:
            filename (str): name of the plot file generated
            data (list): data from the scan
            time (int): duration of the test in seconds, used for the BER limit calculation
        """
        #with open(filename) as f:
        #    data = f.readlines()

        xPoints = np.array([json.loads(x)["CTLEHFSR"] for x in data])
        xPoints = np.sort(np.unique(xPoints))
        yPoints = np.array([json.loads(x)["CTLEMFSR"] for x in data])
        yPoints = np.sort(np.unique(yPoints))
        heatmap = np.array([np.sum(json.loads(x)["results"]) for x in data])
        heatmap = np.ma.masked_where(heatmap<0, heatmap)
        heatmap = np.transpose(np.reshape(heatmap, (len(xPoints), len(yPoints))))

        plt.figure(figsize=(8,6))
        cmap = cm.get_cmap("summer").copy()
        cmap.set_bad('black',1.)
        plt.imshow(heatmap, cmap = cmap, origin="lower", extent=[xPoints[0]-0.5, xPoints[-1] + 0.5, yPoints[0]-0.5, yPoints[-1] + 0.5])
        plt.xticks(xPoints)
        plt.yticks(yPoints)
        plt.ylabel("CTLEMFSR")
        plt.xlabel("CTLEHFSR")
        plt.title("Soft errors scan")
        cbar = plt.colorbar()
        cbar.set_label('Number of soft errors')
        plotname = filename.split(".")[0] + "_plot.png"
        plt.savefig(plotname, format="png")

        poisson_upper_limits = [3.0, 4.74, 6.3, 7.75, 9.15, 10.51, 11.84, 13.15, 14.43, 15.71, 16.96]

        def upperLimit(BERT_error_count):
            if BERT_error_count<0:
                return BERT_error_count
            if BERT_error_count<11:
                mu = poisson_upper_limits[int(BERT_error_count)]
            else:
                mu = BERT_error_count + 1.96*(BERT_error_count)**(1/2)
            return mu

        VupperLimit = np.vectorize(upperLimit)
        heatmap_bert = VupperLimit(heatmap)/(time*62.12/100*1.28*10**9)
        logger.info(heatmap_bert)

        plt.figure(figsize=(8,6))
        cmap = cm.get_cmap("summer").copy()
        cmap.set_bad('black',1.)
        plt.imshow(heatmap_bert, cmap = cmap, norm=colors.LogNorm(), origin="lower", extent=[xPoints[0]-0.5, xPoints[-1] + 0.5, yPoints[0]-0.5, yPoints[-1] + 0.5])
        plt.xticks(xPoints)
        plt.yticks(yPoints)
        plt.ylabel("CTLEMFSR")
        plt.xlabel("CTLEHFSR")
        plt.title("Soft errors BER limit")
        cbar = plt.colorbar()
        cbar.set_label('BER limit')
        plotname = filename.split(".")[0] + "berLimit_plot.png"
        plt.savefig(plotname, format="png")

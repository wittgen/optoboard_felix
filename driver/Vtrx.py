# pylint: disable=line-too-long, invalid-name, import-error
"""VTRx+ module."""

from driver.log import logger
from driver.Hardware import Hardware, ordered_configuration

try:
    from register_maps import vtrx_register_map_v1_2
    from register_maps import vtrx_register_map_v1_3
except (ImportError, ModuleNotFoundError):
    logger.critical("Error in importing VTRX+ register maps!")



class Vtrx(Hardware):
    """Class for controlling the VTRx+ on an Optoboard.

    Attributes:
        device_type (str): "vtrx"
        device (str): same as device_type
        address (hex): I2C address of the VTRx+, identical on all Optoboards
        master (bool): False because it can only be controlled via the lpGBT master
    """

    device_type = "vtrx"
    device = device_type
    address = 0x50
    master = False

    def __init__(self, Communication_wrapper, device, address, master, I2C_master, config_file, vtrx_v, vtrxChannelsOn):
        """Initialize of VTRx+ object.

        Args:
            Communication_wrapper (class): low level access to FELIX access
            device (str): always vtrx
            address (hex): I2C address of the VTRx+
            master (bool): should always be False
            I2C_master (int): I2C controller to use (can be 0, 1, 2)
            config_file (dict): configuration values from JSON config
            vtrx_v (int): VTRx+ quad laser driver version
            vtrxChannelsOn (int): turn on only the vtrx+ channels connected to a mounted lpGBTs, least significant bit corresponds to lpGBT master

        Attributes:
            vtrx_v (str): VTRx+ quad laser driver version, either 1.2 or 1.3
            vtrx_reg_map (class): imported VTRx+ register map
            config_file (dict): configuration values from JSON config
        
        Raises:
            ValueError: no register found in register map that matches the register name provided by the config. Usually this is the case if a wrong version has been provided in a custom configuration.
        """
        super().__init__(Communication_wrapper, device, address, master, I2C_master, config_file)

        logger.debug("Communication_wrapper: %s, device: %s, address: %s, master: %s, I2C_master: %s, vtrx_v: %s",
                     Communication_wrapper, device, address, master, I2C_master, vtrx_v)

        self.vtrx_v = vtrx_v
        self.vtrxChannelsOn = vtrxChannelsOn

        if self.vtrx_v == "1.2":
            self.vtrx_reg_map = vtrx_register_map_v1_2.VTRxplusRegisterMapV1_2
        elif self.vtrx_v == "1.3":
            self.vtrx_reg_map = vtrx_register_map_v1_3.VTRxplusRegisterMapV1_3
        else:
            logger.error("VTRx+ quad laser driver version %s not supported!", self.vtrx_v)

        try:
            self.config_file["vtrx"] = ordered_configuration(self.config_file["vtrx"], self.vtrx_reg_map)
        except AttributeError:
            logger.error("Registers in config do not fit the register map, wrong VTRx+ version %s used!", self.vtrx_v)
            raise ValueError("Wrong VTRx+ version in config")
        self.config_file.pop("power_up_registers_master")
        self.config_file.pop("power_up_registers_slave")
        self.config_file.pop("lpgbt")
        self.config_file.pop("gbcr")

        logger.info("%s object initialised!", self.device)

    def configure(self):
        """Enable all TX fibre channels.

        Configuring all 32 registers of the VTRx+ is currently not included in this method. The VTRx+ comes preconfigured and no other values have been tested so far.

        Returns:
            None
        """
        onlpGBT = ""
        binvtrxChannelsOn = '{:04b}'.format(self.vtrxChannelsOn)
        for i in range(len(binvtrxChannelsOn)):
            if int(binvtrxChannelsOn[len(binvtrxChannelsOn)-(i+1)]):
                onlpGBT += f"{i+1} "

        if self.vtrx_v == "1.3":
            logger.info("Using VTRx+ quad laser driver v1.3, enabling TX fibre channels for lpGBT %s...", onlpGBT)
            self.write_read("GCR", None, self.vtrxChannelsOn)
        else:
            for i in range(len(binvtrxChannelsOn)):
                if not int(binvtrxChannelsOn[len(binvtrxChannelsOn)-(i+1)]):  
                    logger.info("Disabling fiber connected to lpGBT %s", i+1)
                    self.write_read(f"C{i}CR", f"C{i}CEN", 0)
            logger.info("Using VTRx+ quad laser driver v1.2, TX fibres for lpGBT %sare on", onlpGBT)

    def readAll(self):
        """Read out all registers of the VTRx+.

        Returns:
            read (list): read-out registers, values are decimal
        """
        registers = []

        logger.debug("VTRx+ register map: %s", self.vtrx_reg_map.__dict__)

        for reg in self.vtrx_reg_map.__dict__:
            if (reg != "Reg") and (reg[0] != "_"):
                registers.append(reg)

        read = self.read_32bytes()
        logger.debug("vtrx.readAll - registers: %s, read: %s", registers, read)

        return read

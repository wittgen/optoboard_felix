"""Generate markdown pages of the individual modules in this repository."""

from lazydocs import generate_docs      # needs pip install lazydocs

# The parameters of this function correspond to the CLI options
generate_docs(
    ["driver", "register_maps", "additional_driver", "InitOpto"],
    output_path="../optoboard-system-docs/docs/software/optoboard_felix",       # to your local folder of the optoboard system docs directory
    src_base_url="https://gitlab.cern.ch/bat/optoboard_felix/-/tree/master",        # change for specific branch
    overview_file="index",
    watermark=True
    )
